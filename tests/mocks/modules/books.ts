import { Book } from "@core/modules/books/models/book";
import { filesMock } from "./files";
import { genreMock, genresMock } from "./genres";

export const booksMock = [
    {
        title : "Erasé una vez",
        author : "Pepito",
        publicationYear : "1999",
        genre : genreMock,
        numberPages : "122",
        editorial : "Rocas",
        ISSN : "1010241010243",
        language : "Español",
        publicationDate : new Date(1999,1,1),
        condition : "Nuevo",
        price : "32000",
        stock : "5",
        picture : filesMock
    },
    {
        title : "Hello guys",
        author : "Ricky Art",
        publicationYear : "2008",
        genre : genreMock,
        numberPages : "900",
        editorial : "PandaBooks",
        ISSN : "7749583435",
        language : "Inglés",
        publicationDate : new Date(2008,4,6),
        condition : "Nuevo",
        price : "100000",
        stock : "2",
        picture : filesMock
    },
    {
        title : "Le grande",
        author : "Jo Van",
        publicationYear : "2021",
        genre : genreMock,
        numberPages : "324",
        editorial : "Rocas",
        ISSN : "9957461900",
        language : "Francés",
        publicationDate : new Date(2021,3,5),
        condition : "Usado",
        price : "42000",
        stock : "1",
        picture : filesMock
    }  
] as any as Book[];

export const bookMock = booksMock[0];