import { UserData } from "@core/modules/authentication/interfaces/user-data";
import { personMock, personsMock } from "./persons";

export const userAdministratorsMock = [
    {
        username: 'some@email.com',
        password: 'somepassword',
        type: 'admin',
        person: personMock,
    },
    {
        username: 'some1@email.com',
        password: 'somepassword1',
        type: 'admin',
        person: personsMock[1],
    },
    {
        username: 'some2@email.com',
        password: 'somepassword2',
        type: 'admin',
        person: personsMock[1],
    }

]as any as UserData[];

export const userAdministratorMock = userAdministratorsMock[0];
 