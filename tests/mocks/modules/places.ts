import { Place } from "@core/models/place";

export const placesMock = [
    {
        city: 'Dosquebradas',
        state: 'Risaralda'
    },
    {
        city: 'Pereira',
        state: 'Risaralda'
    },
    {
        city: 'Medellín',
        state: 'Antioquia'
    }  
] as any as Place[];

export const placeMock = placesMock[0];