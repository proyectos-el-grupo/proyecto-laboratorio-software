import { Person } from "@core/modules/persons/models/person";
import { genresMock } from "./genres";
import { placeMock, placesMock } from "./places";

export const personsMock = [
    {
        name: 'somename',
        lastname: 'somelastname',
        dni: '01234567',
        birthdate: new Date(1999,1,1),
        birthplace: placeMock,
        address: 'someaddress',
        city: placesMock[0],
        gender: 'Masculino',
        ltPreferences: genresMock,    
    },
    {
        name: 'Fulano',
        lastname: 'De tal',
        dni: '12345678',
        birthdate: new Date(1999,1,1),
        birthplace: placeMock,
        address: 'someaddress',
        city: placesMock[1],
        gender: 'Femenino',
        ltPreferences: genresMock,    
    },
    {
        name: 'Pepito',
        lastname: 'Perez',
        dni: '23456789',
        birthdate: new Date(1999,1,1),
        birthplace: placeMock,
        address: 'someaddress',
        city: placesMock[2],
        gender: 'Masculino',
        ltPreferences: genresMock,    
    },
] as any as Person[];

export const personMock = personsMock[0];
