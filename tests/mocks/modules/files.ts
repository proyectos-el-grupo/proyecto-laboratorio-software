import { File } from "@core/models/file";

export const filesMock = [
    {
        url: "http://somedomain.com/bc9f32df-2957-4bb1-93c9-ec47d9870a05/tfss-db295fb2-8a8b-49f3-aad3-dd911142f64f-hello.jpg",
        name: "db295fb2-8a8b-49f3-aad3-dd911142f64f-hello.jpg"
    },
    {
        url: "http://somedomain.com/bc9f32df-2957-4bb1-93c9-ec47d9870a05/tfss-db31fr2-prueba.png",
        name: "db31fr2-prueba.png"
    },
    {
        url: "http://somedomain.com/bc9f32df-2957-4bb1-93c9-ec47d9870a05/tfss-db554cd-fw1144r-adios.jpeg",
        name: "db554cd-fw1144r-adios.jpeg"
    }  
] as any as File[];

export const fileMock = filesMock[0];