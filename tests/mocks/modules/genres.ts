import { Genre } from "@core/modules/books/models/genre";

export const genresMock = [
    {
        name: 'Aventura',
    },
    {
        name: 'Realismo Mágico',
    },
    {
        name: 'Infantil',
    }  
] as any as Genre[];

export const genreMock = genresMock[0];