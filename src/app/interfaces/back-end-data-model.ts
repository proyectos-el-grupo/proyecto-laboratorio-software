export interface BackEndDataModel {
    readonly objectId?: string;
    readonly createdAt?: Date;
    readonly updatedAt?: Date;
}
