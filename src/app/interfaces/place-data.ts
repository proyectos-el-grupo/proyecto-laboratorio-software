import { BackEndDataModel } from "@core/interfaces/back-end-data-model";

export interface PlaceData extends BackEndDataModel {
    city: string;
    state: string;
}
