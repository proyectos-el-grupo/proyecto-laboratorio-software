import { BackEndDataModel } from "@core/interfaces/back-end-data-model";

export interface FileData{
    url: string;
    name: string;
}
