import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '@core/modules/books/models/book';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: Book[], 
            text: string,
            title: string,
            author?: string,
            price?: string): Book[] {
    if(text === ''){
      return array;
    }

    text = text.toLowerCase();

    const result = [];
    for(const book of array){
      if(book[title].toLowerCase().includes(text)){
        result.push(book);
      }
    }

    for(const book of array){
      if(book[author].toLowerCase().includes(text)){
        result.push(book);
      }
    }

    for(const book of array){
      if(book[price] == text || book[price] <= text){
        result.push(book);
      }
    }

    return result;

  }

}
