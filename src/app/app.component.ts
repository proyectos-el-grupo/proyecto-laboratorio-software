import { Component } from '@angular/core';
import { AuthenticationService } from './modules/authentication/services/authentication.service';
import { LoginService } from './modules/authentication/services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    public authenticationService: AuthenticationService,
    public loginService: LoginService,
  ) { }

  public logOut(){
    this.loginService.logOut();
  }
}
