import { BackEndDataModel } from '../interfaces/back-end-data-model';

/**
 * A base front end model related to a DB registry.
 */
export class FrontEndModel implements BackEndDataModel {
    public readonly id: string;
    public readonly createdAt: Date;
    public readonly updatedAt: Date;

    public get exists(): boolean {
        return Boolean(this.objectId);
    }

    public get objectId() {
        return this.id;
    }

    public constructor(entity: BackEndDataModel) {

        if (entity.objectId) {
            this.id = entity.objectId;
        }

        if (entity.createdAt) {
            this.createdAt = new Date(entity.createdAt);
        }

        if (entity.updatedAt) {
            this.updatedAt = new Date(entity.updatedAt);
        }
    }

    /**
     * return the Back End Properties names in an array
     */
    public nonBackEndProperties(): string[] {
        return ['id', 'createdAt', 'updatedAt'];
    }
}
