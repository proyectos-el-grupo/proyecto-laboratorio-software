import { FrontEndModel } from "@core/models/front-end-model";
import { FileData } from "../interfaces/file-data";

export class File implements FileData {
    public url: string;
    public name: string;

    public constructor(entity: FileData) {
        this.url = entity.url;
        this.name = entity.name;
    }
}
