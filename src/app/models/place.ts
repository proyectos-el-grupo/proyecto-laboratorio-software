import { FrontEndModel } from "@core/models/front-end-model";
import { PlaceData } from "../interfaces/place-data";

export class Place extends FrontEndModel {
    public city: string;
    private state: string;

    public constructor(entity: PlaceData) {
        super(entity);
        this.city = entity.city;
        this.state = entity.state;
    }

    public metodoGenial(): void{
        let a = 1;
        const b = 9;
        while (a < 10) {
            a=a+1;
        }
    }    
}
