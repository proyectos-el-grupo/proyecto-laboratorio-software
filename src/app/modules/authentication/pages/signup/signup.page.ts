import { Component, OnInit } from '@angular/core';

import { Place } from '@core/models/place';
import { Genre } from '@core/modules/books/models/genre';
import { GenresService } from '@core/modules/books/services/genres.service';
import { Person } from '@core/modules/persons/models/person';
import { PlacesService } from '@core/services/places.service';
import { ModalController, ToastController } from '@ionic/angular';
import { User } from '../../models/user';
import { SignupService } from '../../services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  public places : Place[] = [];
  public genres : Genre[] = [];

  constructor(
    protected placesService: PlacesService,
    protected genresService: GenresService,
    protected signupService: SignupService,
    private modalController: ModalController,
    private toastController: ToastController,
  ) { }

  async ngOnInit() {
    this.places = await this.placesService.getAll();
    this.genres = await this.genresService.getAll();
  }

  public async onSignUpUser(signUpData) {
    const person = new Person({
      name: signUpData.name,
      lastname: signUpData.lastname,
      dni: signUpData.dni,
      birthdate: signUpData.birthdate,
      birthplace: signUpData.birthplace,
      address: signUpData.address,
      city: signUpData.city,
      gender: signUpData.gender,
      ltPreferences: signUpData.ltPreferences,
    });

    const user = new User({
      username: signUpData.username,
      password: signUpData.password,
      type: 'client',
      person,
    });

    try {
      await this.signupService.signUp(user);
      this.successSignUp();
    } catch (error) {
      this.handleHttpError(error);
      throw error;
    }
  }

  public goBack() {
    this.modalController.dismiss();
  }

  public async handleHttpError(errorResponse) {
    let message: string;

    if (errorResponse.error.error){
      message = errorResponse.error.error;
    } else {
      message = 'Cannot connect to server';
    }

    const toast = await this.toastController.create({
      message,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async successSignUp(){
    const toast = await this.toastController.create({
      message: 'Registrado exitosamente',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }
}
