import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';

import { genresMock } from 'tests/mocks/modules/genres';
import { Place } from '@core/models/place';
import { placeMock, placesMock } from 'tests/mocks/modules/places';
import { SignupPage } from './signup.page';
import { SignupFormComponent } from '../../components/signup-form/signup-form.component';
import { PlacesService } from '@core/services/places.service';
import { SignupService } from '../../services/signup.service';
import { Genre } from '@core/modules/books/models/genre';
import { GenresService } from '@core/modules/books/services/genres.service';

describe('SignupPage', () => {
  let component: SignupPage;
  let fixture: ComponentFixture<SignupPage>;

  let placesServiceMock: {
    getAll: jasmine.Spy,
  };
  let genresServiceMock: {
    getAll: jasmine.Spy,
  };
  let signupServiceMock: {
    signUp: jasmine.Spy,
  };

  const signupMock = {
    username: 'someusername@domain.com',
    password: 'somepassword',
    repassword: 'somepassword',
    name: 'somename',
    lastname: 'somelastname',
    dni: '01234567',
    birthdate: new Date(1999,1,1),
    birthplace: placeMock,
    address: 'someaddress',
    city: placeMock,
    gender: 'Masculino',
    ltPreferences: genresMock,
  };

  const modeledPlacesMock = placesMock.map (placeData => new Place(placeData));
  const modeledGenresMock = genresMock.map (genreData => new Genre(genreData));

  beforeEach(waitForAsync(() => {
    placesServiceMock = jasmine.createSpyObj('PlacesService', ['getAll']);
    placesServiceMock.getAll.and.returnValue(Promise.resolve(modeledPlacesMock));

    genresServiceMock = jasmine.createSpyObj('GenresService', ['getAll']);
    genresServiceMock.getAll.and.returnValue(Promise.resolve(modeledGenresMock));

    signupServiceMock = jasmine.createSpyObj('SignupService', ['signUp']);

    TestBed.configureTestingModule({
      declarations: [
        SignupPage,
        SignupFormComponent,
      ],
      providers: [
        {provide: PlacesService, useValue: placesServiceMock},
        {provide: GenresService, useValue: genresServiceMock},
        {provide: SignupService, useValue: signupServiceMock},
      ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        RouterTestingModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should try a signup', async () => {
    await component.onSignUpUser(signupMock);

    expect(signupServiceMock.signUp).toHaveBeenCalledTimes(1);
  });

  it('should dismiss modal when cancel button is clicked', () => {
    const button = fixture.nativeElement.querySelector('ion-button[type=\'button\']');
    const goBackSpy = spyOn(component, 'goBack');

    button.click();

    expect(goBackSpy).toHaveBeenCalledTimes(1);
  });

  it('should show a success toast when user is succesfully signed up', async () => {
    const handleHttpErrorSpy = spyOn(component, 'handleHttpError');
    const successSignUpSpy = spyOn(component, 'successSignUp');

    signupServiceMock.signUp.and.returnValue(Promise.resolve(true));

    await component.onSignUpUser(signupMock);

    expect(handleHttpErrorSpy).toHaveBeenCalledTimes(0);
    expect(successSignUpSpy).toHaveBeenCalledTimes(1);
  });

  it('should show an error toast when user is not succesfully signed up', async () => {
    const handleHttpErrorSpy = spyOn(component, 'handleHttpError');
    const successSignUpSpy = spyOn(component, 'successSignUp');

    signupServiceMock.signUp.and.returnValue(Promise.reject());

    try {
      await component.onSignUpUser(signupMock);
    } catch (error) {
      expect(handleHttpErrorSpy).toHaveBeenCalledTimes(1);
      expect(successSignUpSpy).toHaveBeenCalledTimes(0);
    }
  });
});
