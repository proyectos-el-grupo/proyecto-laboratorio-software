import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { AuthenticationService } from '../../services/authentication.service';
import { LoginService } from '../../services/login.service';
import { SignupPage } from '../signup/signup.page';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @Output()
  public isLogged: EventEmitter<{logged: boolean}> = new EventEmitter();

  constructor(
    protected loginService: LoginService,
    private modalController: ModalController,
    private toastController: ToastController,
    private authenticationService: AuthenticationService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.verifySession();
  }

  public verifySession(){
    if(this.authenticationService.isAuthenticated){
        this.router.navigateByUrl('/search-book', {skipLocationChange: false});
    }
  }

  /**
   * Show a toast when there is an non-101 error in log in
   */
   public async handleError() {
    const toast = await this.toastController.create({
      message: 'No es posible iniciar sesión en este momento',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  /**
   * Show a toast when the error is associated with username or password
   */
  public async handleInvalidLoginInfo() {
    const toast = await this.toastController.create({
      message: 'Nombre de usuario y/o contraseña invalidos',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  /**
   * Perform a login
   */
  public async onLogin(user: {username, password}) {
    try {
      await this.loginService.logIn(user.username, user.password);
      this.isLogged.emit({logged: true});
      this.redirectTo();
    } catch (errorResponsive) {
      if (errorResponsive.error.code === 101){
        this.handleInvalidLoginInfo();
      } else {
        this.handleError();
      }
    }
  }

  /**
   * Show a modal with a signup form
   */
  public async goToSignUpForm() {
    const modal = await this.modalController.create({
      component: SignupPage,
    });

    await modal.present();
  }

  public redirectTo(){
    if(this.authenticationService.isAuthenticated){
      const session = this.authenticationService.getSession();
      if(session.type === 'root'){
        // TODO: Imlements the url of assigned page
        this.router.navigateByUrl('/administration');
      }
      if(session.type === 'admin'){
        // TODO: Implements the url of assigned page
        this.router.navigateByUrl('/admin-books');
      }
      if(session.type === 'client'){
        // TODO: Implements the url of assigned page
        this.router.navigateByUrl('/search-book');
      }
    }
  }
}
