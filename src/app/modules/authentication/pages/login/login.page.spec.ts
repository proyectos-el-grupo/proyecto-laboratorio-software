import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { LoginFormComponent } from '../../components/login-form/login-form.component';
import { LoginService } from '../../services/login.service';

import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let loginServiceSpy: {
    logIn: jasmine.Spy,
  };

  const formMock = {
    username: 'fulanito@detal.com',
    password: 'detallomejor',
  }

  beforeEach(waitForAsync(() => {
    loginServiceSpy = jasmine.createSpyObj('LoginService', ['logIn']);
    TestBed.configureTestingModule({
      declarations: [ 
        LoginPage, 
        LoginFormComponent
      ],
      providers:[{
        provide: LoginService,
        useValue: loginServiceSpy
      }],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        RouterTestingModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should try a login',  async ()=>{
    await component.onLogin(formMock);

    expect(loginServiceSpy.logIn).toHaveBeenCalledTimes(1);
  });

  it('should emit a event when the user is succesfully logged', async ()=>{
    const eventEmitter = spyOn(component.isLogged, 'emit');

    await component.onLogin(formMock);

    expect(eventEmitter).toHaveBeenCalledTimes(1);
  });

  it('should show login form', ()=>{
    const componentMock = fixture.nativeElement.querySelector('app-login-form');

    expect(componentMock).toBeTruthy();
  });

  it('should show a modal when sign up invitation is clicked', ()=>{
    const button = fixture.nativeElement.querySelector('ion-text');
    const goToSignUpFormSpy = spyOn(component, 'goToSignUpForm');

    button.click();

    expect(goToSignUpFormSpy).toHaveBeenCalledTimes(1);
  });

  it('should show a toast when there is an non-101 error in log in', async () => {
    const handleErrorSpy = spyOn(component, 'handleError');
    const handleInvalidLoginInfoSpy = spyOn(component, 'handleInvalidLoginInfo');

    const error = {
      error: {
        code: 610,
      }
    };

    loginServiceSpy.logIn.and.returnValue(Promise.reject(error));

    await component.onLogin(formMock);

    expect(handleErrorSpy).toHaveBeenCalledTimes(1);
    expect(handleInvalidLoginInfoSpy).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when there is an error associated to username or password', async () => {
    const handleErrorSpy = spyOn(component, 'handleError');
    const handleInvalidLoginInfoSpy = spyOn(component, 'handleInvalidLoginInfo');

    const error = {
      error: {
        code: 101,
      }
    };

    loginServiceSpy.logIn.and.returnValue(Promise.reject(error));

    await component.onLogin(formMock);

    expect(handleErrorSpy).toHaveBeenCalledTimes(0);
    expect(handleInvalidLoginInfoSpy).toHaveBeenCalledTimes(1);
  });
});
