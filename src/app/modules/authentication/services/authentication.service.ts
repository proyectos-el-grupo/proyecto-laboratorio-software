import { Injectable } from '@angular/core';
import { SessionData } from '../interfaces/session-data';

export const SESSION_KEY = 'authentication.session';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() { }

  /**
  * Return true when user is authenticated
  */
  public get isAuthenticated(): boolean {
    return localStorage.getItem(SESSION_KEY) !== null;
  }

  /**
   * Return the session token from local storage
   */
  public get sessionToken(): string {
    return this.getSession().sessionToken;    
  }

  /**
  * Save the session information into local storage
  */
  public saveSession(sessionData): void {
    localStorage.setItem(SESSION_KEY, JSON.stringify(sessionData));
  }

  /**
  * Get the session information from local storage
  */
  public getSession(): SessionData {
    return JSON.parse(localStorage.getItem(SESSION_KEY));
  }

  /**
   * Delete the session information stored in local storage
   */
  public cleanSession(): void {
    localStorage.removeItem(SESSION_KEY);
  }

  /**
   * Get the user id from local storage
   */
  public get userId(): string{
    return JSON.parse(localStorage.getItem(SESSION_KEY)).objectId;
  }

  /**
   * Get the type form local storage 
   */
  public get userType(): string {
    return JSON.parse(localStorage.getItem(SESSION_KEY)).type;
  }
}
