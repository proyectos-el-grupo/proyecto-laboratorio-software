import { TestBed } from '@angular/core/testing';

import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { LoginService } from './login.service';
import { personMock } from 'tests/mocks/modules/persons';
import { placeMock } from 'tests/mocks/modules/places';
import { genresMock } from 'tests/mocks/modules/genres';

describe('LoginService', () => {
  let service: LoginService;
  let httpClientSpy: {
    get: jasmine.Spy,
    put: jasmine.Spy,
    post: jasmine.Spy
  };
  let personsServiceSpy: {
    getOne: jasmine.Spy,
  };
  let placesServiceSpy: {
    getOne: jasmine.Spy,
  };
  let genresServiceSpy: {
    getLtPreferences: jasmine.Spy,
  }
  let authenticationSpy: {
    sessionToken: jasmine.Spy,
    saveSession: jasmine.Spy,
    cleanSession: jasmine.Spy,
  };

  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'post']);
    personsServiceSpy = jasmine.createSpyObj('PersonsService', ['getOne']);
    placesServiceSpy = jasmine.createSpyObj('PlacesService', ['getOne']);
    genresServiceSpy = jasmine.createSpyObj(['GenresService', ['getLtPreferences']]);
    authenticationSpy = jasmine.createSpyObj('AuthenticationService',[
                                                                      'saveSession', 
                                                                      'sessionToken',
                                                                      'cleanSession'
                                                                    ]);

    TestBed.configureTestingModule({});
    service = new LoginService(
      httpClientSpy as any,
      personsServiceSpy as any,
      placesServiceSpy as any,
      genresServiceSpy as any,
      authenticationSpy as any,
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it ('should login a client user', async () => {
    let returnedObject;

    const username = 'cooldude6@cooldomain.com';
    const type = 'client';
    const password = 'S0meÑP@ssword';

    const sessionMock = {
      username,
      type,
      person: personMock,
      objectId: 'g7y9tkhB7O',
      sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
    };

    const params = {
      username,
      password,
    };

    const endpoint = API_BASE + '/login';

    httpClientSpy.get.and.returnValue(asyncData(sessionMock));
    personsServiceSpy.getOne.and.returnValue(Promise.resolve(personMock));
    placesServiceSpy.getOne.and.returnValue(Promise.resolve(placeMock));
    genresServiceSpy.getLtPreferences.and.returnValue(Promise.resolve(genresMock));

    returnedObject = await service.logIn(username, password);

    expect(returnedObject).toEqual(sessionMock);
    expect(returnedObject.person).toEqual(personMock);
    expect(returnedObject.person.city).toEqual(placeMock);
    expect(returnedObject.person.ltPreferences).toEqual(genresMock);
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders, params});
  });

  it('should logout an user', async ()=>{
    authenticationSpy.sessionToken.and.returnValue('r:pnktnjyb996sj4p156gjtp4im');

    const sessionToken = authenticationSpy.sessionToken;

    const sessionTokenHeader = {
      'X-Parse-Session-Token' : sessionToken,
    };

    const headers = Object.assign(sessionTokenHeader, API_HEADERS);

    const options = {
      headers,
    };

    const endpoint = API_BASE + '/logout';

    await service.logOut();

    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual(options);
  });
});
