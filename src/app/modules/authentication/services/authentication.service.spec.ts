import { TestBed } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  const personMock = {
    dni: '1088357275',
    name: 'Niko',
    lastname: 'Elriko',
    birthdate: '1999-10-06',
    birthplace: 'Pereira',
    address: 'mz busquela casa encuentrela',
    city: 'dosquebradas',
    gender: 'male',
    mobile: '3200000000',
    ltPreference: 'null',
  } as any;

  const loginMock = {
    username: 'cooldude6@cooldomain.com',
    type: 'client',
    person: personMock,
    objectId: 'g7y9tkhB7O',
    sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthenticationService);
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return false when session is not started', () => {
    const isSessionStarted = service.isAuthenticated;

    expect(isSessionStarted).toBeFalse();
  });

  it('should retrieve session information from local storage', () => {
    service.saveSession(loginMock);

    const session = service.getSession();

    expect(session).toEqual(loginMock);
  });
  
  it('should return the user id', ()=>{
    service.saveSession(loginMock);

    const userId = service.userId;

    expect(userId).toEqual(loginMock.objectId);
  });
});
