import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Place } from '@core/models/place';
import { Genre } from '@core/modules/books/models/genre';
import { GenresService } from '@core/modules/books/services/genres.service';

import { PersonsService } from '@core/modules/persons/services/persons.service';
import { PlacesService } from '@core/services/places.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { SessionData } from '../interfaces/session-data';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    protected httpClient: HttpClient,
    private personsService: PersonsService,
    private placesService: PlacesService,
    private genresService: GenresService,
    private authentication: AuthenticationService,
  ) { }

  /**
   * Allow an user to login in the app
   */
   public logIn(username: string, password: string): Promise<SessionData> {
    const APIEndPoint = API_BASE + '/login';
    const params = {
      username,
      password
    };
    const options = {
      headers: API_HEADERS,
      params,
    };

    let session: SessionData;

    if(username === 'root@root.com') {
      return this.httpClient.get<SessionData>(APIEndPoint, options)
      .toPromise()
      .then(loadedSession => {
        session = loadedSession;
        this.authentication.saveSession(session);
        return session;
      });
    } else {
      return this.httpClient.get<SessionData>(APIEndPoint, options)
    .toPromise()
    .then(loadedSession => {
      session = loadedSession;

      return this.personsService.getOne(session.person.objectId);
    }).then(person =>{
      session.person = person;

      if (session.type === 'client') {
        return this.placesService.getOne(person.birthplace.objectId);
      }
    }).then(birthplace =>{
      if (session.type === 'client') {
        session.person.birthplace = birthplace;  
        return this.genresService.getLtPreferences(session.person.ltPreferences);
      }
    }).then(ltPreferences =>{
      if (session.type === 'client') {
        session.person.ltPreferences = ltPreferences;
        return this.placesService.getOne(session.person.city.objectId);
      }
    }).then(city =>{
      if (session.type === 'client') {
        session.person.city = city;
        this.authentication.saveSession(session);
        return session;
      } else {
        this.authentication.saveSession(session);
        return session;
      }
    });
    }
  }

    public logOut(){
      const sessionToken = this.authentication.sessionToken;
      const APIEndPoint = API_BASE + '/logout';
      const sessionTokenHeader = {
        'X-Parse-Session-Token' : sessionToken,
      };
      const headers = Object.assign(sessionTokenHeader, API_HEADERS);
      const options = {
        headers
      };

      this.httpClient.post(APIEndPoint, null, options);

      this.authentication.cleanSession();
    }
}
