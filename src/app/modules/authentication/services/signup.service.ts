import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdministrationService } from '@core/modules/administration/services/administration.service';

import { PersonsService } from '@core/modules/persons/services/persons.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(
    protected httpClient: HttpClient,
    protected personsService: PersonsService,
    protected administrationService: AdministrationService,
  ) { }

  /**
   * Allow an user to sign up
   */
  public async signUp(user: User): Promise<any>{
    const APIEndPointUser = API_BASE + '/users';
    const APIHeader = {
      headers: API_HEADERS,
    };
    let response: any;
    let userPassword: string;

    if(user.type === 'admin'){
      response = await this.personsService.createAdmin(user.person);
      userPassword = this.administrationService.randomPassword(10);
    }else{
      response = await this.personsService.createPerson(user.person);
      userPassword = user.password;
    }
    const backendUser = {
      username: user.username,
      password: userPassword,
      type: user.type,
      person: {
        __type: 'Pointer',
        className: 'Person',
        objectId: response.objectId,
      },
    };

    return this.httpClient.post(APIEndPointUser, backendUser, APIHeader).toPromise();
  }
}
