import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { Person } from '@core/modules/persons/models/person';

export interface SessionData extends BackEndDataModel {
    username: string;
    type: string;
    person: Person;
    sessionToken: string;
}
