import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { PlaceData } from '@core/interfaces/place-data';
import { Place } from '@core/models/place';
import { GenreData } from '@core/modules/books/interfaces/genre-data';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss'],
})
export class SignupFormComponent implements OnChanges {
  @Input()
  public places: PlaceData[];
  @Input()
  public genres: GenreData[];

  @Output()
  public submitForm: EventEmitter<{
    username: string,
    password: string,
    repassword: string,
    name: string,
    lastname: string,
    dni: string,
    birthdate: Date,
    birthplace: Place,
    address: string,
    city: Place,
    gender: string,
    ltPreferences: any,
  }> = new EventEmitter();

  public states;
  public formBirthplaces;
  public cities;
  public signupForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public toastController: ToastController,
  ) {
    this.signupForm = this.formBuilder.group({
      username: ['', [
        Validators.minLength(5),
        Validators.maxLength(60),
        Validators.email,
        Validators.required
      ]],
      password: ['', [
        Validators.minLength(6),
        Validators.maxLength(24),
        Validators.required
      ]],
      repassword: ['', [
        Validators.minLength(6),
        Validators.maxLength(24),
        Validators.required
      ]],
      name: ['', [
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.required
      ]],
      lastname: ['', [
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.required
      ]],
      dni: ['', [Validators.minLength(6), 
        Validators.maxLength(10), 
        Validators.pattern('^[0-9,$]*$'),
        Validators.required,]
      ],
      birthdate: ['', Validators.required],
      birthplace: ['', Validators.required],
      address: ['', [
        Validators.minLength(6),
        Validators.maxLength(64),
        Validators.required
      ]],
      city: ['', Validators.required],
      gender: ['', Validators.required],
      ltPreferences: [''],
    });
  }

  ngOnChanges() {
    this.states = this.getStates();
  }

  public onSubmit(): void{
    if (!this.signupForm.valid){
      this.invalidFormToast();
      return;
    }
    if (this.signupForm.value.password !== this.signupForm.value.repassword){
      this.notMatchPasswordToast();
      return;
    }

    const value = this.signupForm.value;

    this.submitForm.emit(value);
  }

  public getBirthplaces(state: any): void{
    this.formBirthplaces=this.places.filter(city => city.state == state.detail.value);
  }

  public getCities(state: any): void{
    this.cities=this.places.filter(city => city.state == state.detail.value);
  }

  public async invalidFormToast() {
    const toast = await this.toastController.create({
      message: 'Faltan campos o la información es incorrecta',
      duration: 2000,
    });
    toast.present();
  }

  public async notMatchPasswordToast() {
    const toast = await this.toastController.create({
      message: 'Las contraseñas no coinciden',
      duration: 2000,
    });
    toast.present();
  }

  private getStates(){
    let states = [];
    this.places.forEach((element) => {
      if (!states.includes(element.state)){
        states.push(element.state);
      }
    });
    states.sort();
    return states;
  }

}
