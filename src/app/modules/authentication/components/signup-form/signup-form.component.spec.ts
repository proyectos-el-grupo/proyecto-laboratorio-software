import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { genresMock } from 'tests/mocks/modules/genres';
import { placeMock, placesMock } from 'tests/mocks/modules/places';

import { SignupFormComponent } from './signup-form.component';

describe('SignupFormComponent', () => {
  let component: SignupFormComponent;
  let fixture: ComponentFixture<SignupFormComponent>;

  let signupMock = {
    username: 'someusername@domain.com',
    password: 'somepassword',
    repassword: 'somepassword',
    name: 'somename',
    lastname: 'somelastname',
    dni: '01234567',
    birthdate: new Date(1999,1,1),
    birthplace: placeMock,
    address: 'someaddress',
    city: placeMock,
    gender: 'Masculino',
    ltPreferences: genresMock,
  }

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupFormComponent ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SignupFormComponent);
    component = fixture.componentInstance;
    component.places = placesMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when form is empty', () => {
    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should not emit information when submit button is clicked and passwords do not match', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const signupFormEmmiter = spyOn(component.submitForm, 'emit');

    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const name = component.signupForm.controls.name;
    const lastname = component.signupForm.controls.lastname;
    const dni = component.signupForm.controls.dni;
    const birthdate = component.signupForm.controls.birthdate;
    const birthplace = component.signupForm.controls.birthplace;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const gender = component.signupForm.controls.gender;
    const ltPreferences = component.signupForm.controls.ltPreferences;

    username.setValue(signupMock.username);
    password.setValue(signupMock.password);
    repassword.setValue('poncioPilato');
    name.setValue(signupMock.name);
    lastname.setValue(signupMock.lastname);
    dni.setValue(signupMock.dni);
    birthdate.setValue(signupMock.birthdate);
    birthplace.setValue(signupMock.birthplace);
    address.setValue(signupMock.address);
    city.setValue(signupMock.city);
    gender.setValue(signupMock.gender);
    ltPreferences.setValue(signupMock.ltPreferences);

    button.click();

    expect(signupFormEmmiter).toHaveBeenCalledTimes(0);
  });

  it('should emit information when submit button is clicked, passwords match and form is valid', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const signupFormEmmiter = spyOn(component.submitForm, 'emit');

    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const name = component.signupForm.controls.name;
    const lastname = component.signupForm.controls.lastname;
    const dni = component.signupForm.controls.dni;
    const birthdate = component.signupForm.controls.birthdate;
    const birthplace = component.signupForm.controls.birthplace;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const gender = component.signupForm.controls.gender;
    const ltPreferences = component.signupForm.controls.ltPreferences;

    username.setValue(signupMock.username);
    password.setValue(signupMock.password);
    repassword.setValue(signupMock.repassword);
    name.setValue(signupMock.name);
    lastname.setValue(signupMock.lastname);
    dni.setValue(signupMock.dni);
    birthdate.setValue(signupMock.birthdate);
    birthplace.setValue(signupMock.birthplace);
    address.setValue(signupMock.address);
    city.setValue(signupMock.city);
    gender.setValue(signupMock.gender);
    ltPreferences.setValue(signupMock.ltPreferences);

    button.click();

    expect(signupFormEmmiter).toHaveBeenCalledTimes(1);
    expect(signupFormEmmiter).toHaveBeenCalledWith(signupMock);
  });

  it('should show an invalid information toast when form is invalid', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const signupFormEmmiter = spyOn(component.submitForm, 'emit');
    const invalidFormToastSpy = spyOn(component, 'invalidFormToast');
    const notMatchPasswordToastSpy = spyOn(component, 'notMatchPasswordToast');

    button.click();

    expect(signupFormEmmiter).toHaveBeenCalledTimes(0);
    expect(notMatchPasswordToastSpy).toHaveBeenCalledTimes(0);
    expect(invalidFormToastSpy).toHaveBeenCalledTimes(1);
  });

  it('should show a toast when passwords do not match', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const signupFormEmmiter = spyOn(component.submitForm, 'emit');
    const invalidFormToastSpy = spyOn(component, 'invalidFormToast');
    const notMatchPasswordToastSpy = spyOn(component, 'notMatchPasswordToast');

    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const name = component.signupForm.controls.name;
    const lastname = component.signupForm.controls.lastname;
    const dni = component.signupForm.controls.dni;
    const birthdate = component.signupForm.controls.birthdate;
    const birthplace = component.signupForm.controls.birthplace;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const gender = component.signupForm.controls.gender;
    const ltPreferences = component.signupForm.controls.ltPreferences;

    username.setValue(signupMock.username);
    password.setValue(signupMock.password);
    repassword.setValue('poncioPilato');
    name.setValue(signupMock.name);
    lastname.setValue(signupMock.lastname);
    dni.setValue(signupMock.dni);
    birthdate.setValue(signupMock.birthdate);
    birthplace.setValue(signupMock.birthplace);
    address.setValue(signupMock.address);
    city.setValue(signupMock.city);
    gender.setValue(signupMock.gender);
    ltPreferences.setValue(signupMock.ltPreferences);

    button.click();

    expect(signupFormEmmiter).toHaveBeenCalledTimes(0);
    expect(notMatchPasswordToastSpy).toHaveBeenCalledTimes(1);
    expect(invalidFormToastSpy).toHaveBeenCalledTimes(0);
  });
});
