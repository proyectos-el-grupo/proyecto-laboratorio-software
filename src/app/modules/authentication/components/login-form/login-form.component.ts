import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @Output()
  public submitForm: EventEmitter<{username: string, password: string}> = new EventEmitter();
  
  public loginForm: FormGroup;

  public constructor(
    private formBuilder: FormBuilder,
    public toastController: ToastController,
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', [
        Validators.minLength(5),
        Validators.maxLength(60),
        Validators.email,
        Validators.required
      ]],
      password: ['', [
        Validators.minLength(6),
        Validators.maxLength(24),
        Validators.required
      ]],
    });
  }

  ngOnInit() {}

  public async onSubmit(): Promise<void> {
    if (!this.loginForm.valid){
      if (this.loginForm.controls.password.hasError('required')){
        await this.handlePasswordAbsent();
        return;
      } else if (this.loginForm.controls.username.errors.email){
        await this.handleUsernameMisspelled();
        return;
      } else {
        await this.handleOthersErrors();
        return;
      }
    }

    const value = this.loginForm.value;

    this.submitForm.emit(value);
  }

  public async handlePasswordAbsent() {
    const toast = await this.toastController.create({
      message: 'Debe escribir una contraseña.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handleUsernameMisspelled() {
    const toast = await this.toastController.create({
      message: 'Nombre de usuario debe ser un correo.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handleOthersErrors() {
    const toast = await this.toastController.create({
      message: 'Información incorrecta.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }
}
