import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-change-password-form',
  templateUrl: './change-password-form.component.html',
  styleUrls: ['./change-password-form.component.scss'],
})
export class ChangePasswordFormComponent implements OnInit {
  @Output()
  public submitForm: EventEmitter<{currentPassword: string, newPassword: string, currentNewPassword: string}> = new EventEmitter();

  public changePasswordForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public toastController: ToastController,
    ) {
    this.changePasswordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required],
    });
   }

  ngOnInit() {}

  public async notMatchPasswordToast() {
    const toast = await this.toastController.create({
      message: 'Las contraseñas no coinciden',
      duration: 2000,
    });
    toast.present();
  }

  public async invalidFormToast() {
    const toast = await this.toastController.create({
      message: 'Todos los campos deben ser rellenados',
      duration: 2000,
    });
    toast.present();
  }

  public onSubmit(): void {
    const value = this.changePasswordForm.value;

    if (!this.changePasswordForm.valid) {
      this.invalidFormToast();
      return;
    }

    if (value.newPassword != value.confirmNewPassword) {
      this.notMatchPasswordToast();
      return;
    }

    this.submitForm.emit(value);

  }

}
