import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ChangePasswordFormComponent } from './change-password-form.component';

describe('ChangePasswordFormComponent', () => {
  let component: ChangePasswordFormComponent;
  let fixture: ComponentFixture<ChangePasswordFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePasswordFormComponent ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ChangePasswordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when form is empty', () => {
    const form = component.changePasswordForm;
    expect(form.status).toBe('INVALID');
  });

  it('should not emit information when submit button is clicked and passwords do not match', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const changePasswordFormEmmiter = spyOn(component.submitForm, 'emit');

    const currentPassword = component.changePasswordForm.controls.currentPassword;
    const newPassword = component.changePasswordForm.controls.newPassword;
    const confirmNewPassword = component.changePasswordForm.controls.confirmNewPassword;

    currentPassword.setValue('C0ntrasenia');
    newPassword.setValue('C0ntraseniaNueva');
    confirmNewPassword.setValue('C0ntraseniaNueva1');

    button.click();

    expect(changePasswordFormEmmiter).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when submit button is clicked and passwords do not match', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const notMatchPasswordToastSpy = spyOn(component, 'notMatchPasswordToast');

    const currentPassword = component.changePasswordForm.controls.currentPassword;
    const newPassword = component.changePasswordForm.controls.newPassword;
    const confirmNewPassword = component.changePasswordForm.controls.confirmNewPassword;

    currentPassword.setValue('C0ntrasenia');
    newPassword.setValue('C0ntraseniaNueva');
    confirmNewPassword.setValue('C0ntraseniaNueva1');

    button.click();

    expect(notMatchPasswordToastSpy).toHaveBeenCalledTimes(1);
  });

  it('should show a toast when the form is invalid', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const invalidFormToastSpy = spyOn(component, 'invalidFormToast');

    button.click();

    expect(invalidFormToastSpy).toHaveBeenCalledTimes(1);
  });

  it('should emmit the form when it is complete and passwords match', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const changePasswordFormEmmiter = spyOn(component.submitForm, 'emit');

    const currentPassword = component.changePasswordForm.controls.currentPassword;
    const newPassword = component.changePasswordForm.controls.newPassword;
    const confirmNewPassword = component.changePasswordForm.controls.confirmNewPassword;

    const formMock = {
      currentPassword: 'C0ntrasenia',
      newPassword: 'C0ntraseniaNueva',
      confirmNewPassword: 'C0ntraseniaNueva'
    } as any;

    currentPassword.setValue(formMock.currentPassword);
    newPassword.setValue(formMock.newPassword);
    confirmNewPassword.setValue(formMock.confirmNewPassword);

    button.click();

    expect(changePasswordFormEmmiter).toHaveBeenCalledTimes(1);
    expect(changePasswordFormEmmiter).toHaveBeenCalledWith(formMock);
  });

});
