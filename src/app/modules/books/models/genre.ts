import { FrontEndModel } from "@core/models/front-end-model";
import { GenreData } from "../interfaces/genre-data";

export class Genre extends FrontEndModel implements GenreData {
    public name: string;

    public constructor(entity: GenreData){
        super(entity);
        this.name = entity.name;
    }
}
