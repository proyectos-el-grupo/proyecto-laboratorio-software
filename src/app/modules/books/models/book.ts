import { FrontEndModel } from "@core/models/front-end-model";
import { Genre } from "@core/modules/books/models/genre";
import { File } from "@core/models/file";
import { BookData } from "../interfaces/book-data";

export class Book extends FrontEndModel implements BookData {
    public title : string;
    public author : string;
    public publicationYear : number;
    public genre : Genre;
    public numberPages : number;
    public editorial : string;
    public ISSN : string;
    public language : string;
    public publicationDate : Date;
    public condition : string;
    public price : number;
    public stock : number;
    public picture: File;

    public get soldOut() {
        if (this.stock > 0){
            return false;
        } else {
            return true;
        }
    }

    public constructor(entity: BookData) {
        super(entity);
        this.title = entity.title;
        this.author = entity.author;
        this.publicationYear = entity.publicationYear;
        this.genre = entity.genre;
        this.numberPages = entity.numberPages;
        this.editorial = entity.editorial;
        this.ISSN = entity.ISSN;
        this.language = entity.language;
        this.publicationDate = entity.publicationDate;
        this.condition= entity.condition;
        this.price = entity.price;
        this.stock = entity.stock;
        this.picture = entity.picture;
    }
}
