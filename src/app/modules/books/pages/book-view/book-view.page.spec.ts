import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { bookMock } from 'tests/mocks/modules/books';
import { BookViewComponent } from '../../components/book-view/book-view.component';

import { BookViewPage } from './book-view.page';

describe('BookViewPage', () => {
  let component: BookViewPage;
  let fixture: ComponentFixture<BookViewPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BookViewPage, BookViewComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookViewPage);
    component = fixture.componentInstance;
    component.book = bookMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
