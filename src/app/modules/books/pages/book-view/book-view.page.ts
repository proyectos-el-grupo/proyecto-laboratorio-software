import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BookData } from '../../interfaces/book-data';
import { Book } from '../../models/book';

@Component({
  selector: 'app-book-view-page',
  templateUrl: './book-view.page.html',
  styleUrls: ['./book-view.page.scss'],
})
export class BookViewPage implements OnInit {
  @Input()
  public book: Book;

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  public goBack() {
    this.modalController.dismiss();
  }
}
