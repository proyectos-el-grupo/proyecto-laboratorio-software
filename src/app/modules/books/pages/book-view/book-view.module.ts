import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookViewPageRoutingModule } from './book-view-routing.module';

import { BookViewPage } from './book-view.page';
import { BookViewComponent } from '../../components/book-view/book-view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookViewPageRoutingModule
  ],
  declarations: []
})
export class BookViewPageModule {}
