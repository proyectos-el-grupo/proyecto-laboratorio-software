import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { booksMock } from 'tests/mocks/modules/books';
import { AdminBookCardComponent } from '../../components/admin-book-card/admin-book-card.component';
import { BookListComponent } from '../../components/book-list/book-list.component';
import { Book } from '../../models/book';
import { BooksService } from '../../services/books.service';

import { AdminBooksPage } from './admin-books.page';

describe('AdminBooksPage', () => {
  let component: AdminBooksPage;
  let fixture: ComponentFixture<AdminBooksPage>;
  let booksServiceMock: {
    getAll: jasmine.Spy,
  };

  const modeledBooksMock = booksMock.map (bookData => new Book(bookData));

  beforeEach(waitForAsync(() => {
    booksServiceMock = jasmine.createSpyObj('BooksService', ['getAll']);
    booksServiceMock.getAll.and.returnValue(Promise.resolve(modeledBooksMock)); 
    TestBed.configureTestingModule({
      declarations: [ AdminBooksPage, AdminBookCardComponent, BookListComponent ],
      providers: [{provide: BooksService, useValue: booksServiceMock}],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminBooksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
