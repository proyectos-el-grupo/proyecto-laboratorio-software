import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { ModalController } from '@ionic/angular';
import { Book } from '../../models/book';
import { BooksService } from '../../services/books.service';
import { BookViewPage } from '../book-view/book-view.page';

@Component({
  selector: 'app-admin-books',
  templateUrl: './admin-books.page.html',
  styleUrls: ['./admin-books.page.scss'],
})
export class AdminBooksPage implements OnInit {
  public books: Book[];

  constructor(
    protected booksService: BooksService,
    public modalController: ModalController,
    private authenticationService: AuthenticationService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.verifySession();
    this.loadBooks();
  }

  public verifySession(){
    if(this.authenticationService.isAuthenticated && 
       this.authenticationService.userType === 'root'){
      this.router.navigateByUrl('/search-book', {skipLocationChange: false});

    }else if(!this.authenticationService.isAuthenticated){
      this.router.navigateByUrl('/search-book', {skipLocationChange: false});
    }
  } 

  private loadBooks(){
    this.booksService.getAll()
      .then(backBooks => this.books = backBooks)
      .catch(error => {});
  }

  public async showBookPreview(book){
    console.log(book);
    const modal = await this.modalController.create({
      component: BookViewPage,
      componentProps: {book},
    });
    await modal.present();
  }
}
