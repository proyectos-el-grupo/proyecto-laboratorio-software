import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminBooksPageRoutingModule } from './admin-books-routing.module';

import { AdminBooksPage } from './admin-books.page';
import { AdminBookCardComponent } from '../../components/admin-book-card/admin-book-card.component';
import { BookListComponent } from '../../components/book-list/book-list.component';
import { BookViewPage } from '../book-view/book-view.page';
import { BookViewComponent } from '../../components/book-view/book-view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AdminBooksPageRoutingModule
  ],
  declarations: [AdminBooksPage, AdminBookCardComponent, BookListComponent],
  entryComponents: [
    BookViewPage,
  ],
})
export class AdminBooksPageModule {}
