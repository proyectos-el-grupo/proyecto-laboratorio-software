import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminBooksPage } from './admin-books.page';

const routes: Routes = [
  {
    path: '',
    component: AdminBooksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminBooksPageRoutingModule {}
