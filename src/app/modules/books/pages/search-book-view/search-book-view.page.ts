import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../../models/book';

@Component({
  selector: 'app-search-book-view-page',
  templateUrl: './search-book-view.page.html',
  styleUrls: ['./search-book-view.page.scss'],
})
export class SearchBookViewPage implements OnInit {
  public book: Book;

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
    this.fill();
  }

  public fill(){
    if(this.router.getCurrentNavigation().extras.state){
      this.book = this.router.getCurrentNavigation().extras.state.book;
    }
  }
}
