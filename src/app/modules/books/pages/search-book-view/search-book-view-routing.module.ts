import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchBookViewPage } from './search-book-view.page';

const routes: Routes = [
  {
    path: '',
    component: SearchBookViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchBookViewPageRoutingModule {}
