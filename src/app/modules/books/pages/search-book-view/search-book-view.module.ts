import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchBookViewPageRoutingModule } from './search-book-view-routing.module';

import { SearchBookViewPage } from './search-book-view.page';
import { BookViewComponent } from '../../components/book-view/book-view.component';
import { BookViewPage } from '../book-view/book-view.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchBookViewPageRoutingModule
  ],
  declarations: [
    SearchBookViewPage,
    BookViewPage,
    BookViewComponent,
  ]
})
export class SearchBookViewPageModule {}
