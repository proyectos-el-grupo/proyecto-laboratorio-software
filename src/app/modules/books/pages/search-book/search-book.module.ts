import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchBookPageRoutingModule } from './search-book-routing.module';

import { SearchBookPage } from './search-book.page';
import { PipesModule } from '@core/pipes/pipes.module';
import { SearchBookCardComponent } from '../../components/search-book-card/search-book-card.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchBookPageRoutingModule,
    PipesModule,
  ],
  declarations: [
    SearchBookPage,
    SearchBookCardComponent
  ]
})
export class SearchBookPageModule {}
