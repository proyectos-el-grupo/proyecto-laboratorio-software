import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { PipesModule } from '@core/pipes/pipes.module';
import { IonicModule } from '@ionic/angular';

import { SearchBookPage } from './search-book.page';

describe('SearchBookPage', () => {
  let component: SearchBookPage;
  let fixture: ComponentFixture<SearchBookPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBookPage ],
      imports: [
        IonicModule.forRoot(),
        HttpClientModule,
        PipesModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchBookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
