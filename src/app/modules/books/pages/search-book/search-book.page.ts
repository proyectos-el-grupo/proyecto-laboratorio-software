import { Component, OnInit } from '@angular/core';
import { Book } from '../../models/book';
import { BooksService } from '../../services/books.service';

@Component({
  selector: 'app-search-book-page',
  templateUrl: './search-book.page.html',
  styleUrls: ['./search-book.page.scss'],
})
export class SearchBookPage implements OnInit {
  public books: Book[];
  public searchText: string = '';

  constructor(
    public booksService: BooksService
  ) {}

  ngOnInit() {
    this.load();
  }

  public async load(){
    this.books = await this.booksService.getAll();
  }

  public search(data: any){
    this.searchText = data.detail.value;
  }
}
