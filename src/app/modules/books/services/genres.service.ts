import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DataAPIService } from '@core/services/data-api.service';
import { Genre } from '../models/genre';

@Injectable({
  providedIn: 'root'
})
export class GenresService extends DataAPIService<Genre> {
  public get className() {
    return 'Genre';
  }

  public modelConstructor<GenreData>(data) {
    return new Genre(data);
  }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data)
  }

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  public async getLtPreferences(ltPreferences: any[]): Promise<Genre[]>{
    return new Promise(function(resolve,reject) {
      const loadedLtPreferences = [];
      ltPreferences.forEach(async element => {
        const preference = await this.getOne(element.objectId);
        loadedLtPreferences.push(preference);
      });

      resolve(loadedLtPreferences);
    });
  }
}
