import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DataAPIService } from '@core/services/data-api.service';
import { FilesService } from '@core/services/files.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { Book } from '../models/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService extends DataAPIService<Book> {

  public get className() {
    return 'Book';
  }

  public modelConstructor<BookData>(data) {
    return new Book(data);
  }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data)
  }

  constructor(
    protected httpClient: HttpClient,
    protected filesService: FilesService,
    ) {
    super(httpClient);
  }

  /**
   * createBook
  */
  public async createBook(data) {
    const APIEndPoint = API_BASE + '/classes/' + this.className;
    const APIHeader = {
      headers: API_HEADERS,
    };
    const __type = 'File';

    let backendPicture = await this.filesService.savePicture(data.picture);
    const picture = {
      url: backendPicture.url,
      name: backendPicture.name,
      __type: 'File',
    }

    const backendBook = {
      title: data.title,
      author: data.author,
      publicationYear: data.publicationYear,
      genre: data.genre,
      numberPages: data.numberPages,
      editorial: data.editorial,
      ISSN: data.ISSN,
      language: data.language,
      publicationDate: data.publicationDate,
      condition: data.condition,
      price: data.price,
      stock: data.stock,
      picture,
    }

    return this.httpClient.post<{objectId}>(APIEndPoint, backendBook, APIHeader)
      .toPromise();
  }
}
