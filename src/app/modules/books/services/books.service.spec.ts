import { FileData } from '@core/interfaces/file-data';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { bookMock, booksMock } from 'tests/mocks/modules/books';
import { fileMock } from 'tests/mocks/modules/files';
import { BookData } from '../interfaces/book-data';
import { Book } from '../models/book';

import { BooksService } from './books.service';

describe('BooksService', () => {
  let httpClientSpy: { post: jasmine.Spy, put: jasmine.Spy, get: jasmine.Spy, delete: jasmine.Spy };
  let filesServiceSpy: { savePicture: jasmine.Spy };
  let service: BooksService;

  const className = 'Book';
  const APIEndpoint = API_BASE + '/classes/' + className;
  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'post', 'delete']);
    filesServiceSpy = jasmine.createSpyObj('FilesService', ['savePicture']);
    service = new BooksService(httpClientSpy as any, filesServiceSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save using POST if the object does not exists', async () => {
    let returnedObject;

    const rawMock = booksMock[0];
    const mock = new Book(rawMock);
    const endpoint = APIEndpoint;

    httpClientSpy.post.and.returnValue(asyncData(mock));

    returnedObject = await service.save(mock);

    expect(returnedObject).toEqual(mock);
    expect(httpClientSpy.put).toHaveBeenCalledTimes(0);
    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.post.calls.first().args[1]).toEqual(rawMock);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
  });

  it('should save using PUT if the object exists', async () => {
    const rawMock = bookMock;

    let returnedObject;
    let existentObject = Object.assign({
        objectId: '1a1a1a1a1a1a1a1a1a1a1a1a'
    }, rawMock) as any;

    existentObject = new Book(existentObject);

    const endpoint = APIEndpoint + '/' + existentObject.id;

    httpClientSpy.put.and.returnValue(asyncData(existentObject));

    returnedObject = await service.save(existentObject);

    expect(returnedObject).toEqual(existentObject);
    expect(httpClientSpy.post).toHaveBeenCalledTimes(0);
    expect(httpClientSpy.put).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.put.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.put.calls.first().args[1]).toEqual(rawMock);
    expect(httpClientSpy.put.calls.first().args[2]).toEqual({headers: APIHeaders});
  });

  it('should return a single object with the id', async ()=>{
    const objectId = '1212121212121212121212';
    const endpoint = APIEndpoint + '/' + objectId;
    const mock = Object.assign({objectId}, bookMock) as BookData;

    let returnedObject;

    httpClientSpy.get.and.returnValue(asyncData(mock));

    returnedObject = await service.getOne(objectId);

    expect(returnedObject).toBeInstanceOf(Book);
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('should return expected objects from server', async ()=>{
    const booksResults = {
      results: booksMock,
    };

    let returnedObjects;

    httpClientSpy.get.and.returnValue(asyncData(booksResults));

    returnedObjects = await service.getAll();

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(Book);
      expect(element).toEqual(jasmine.objectContaining(booksMock[i]));
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(APIEndpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
  });

  it('should delete a specific object when resquested by id', async ()=>{
    const id = '1212';
    const endpoint = APIEndpoint + '/' + id;

    httpClientSpy.delete.and.returnValue(Promise.resolve('true'));

    await service.deleteOne(id);

    expect(httpClientSpy.delete).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.delete.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.delete.calls.first().args[1]).toEqual({headers: APIHeaders});
  });

  it('should create a book with an association to a File', async () => {
    const pictureMock = {
      url: fileMock.url,
      name: fileMock.name,
      __type: 'File',
    };
    const dataMock = {
      title: bookMock.title,
      author: bookMock.author,
      publicationYear: bookMock.publicationYear,
      genre: bookMock.genre,
      numberPages: bookMock.numberPages,
      editorial: bookMock.editorial,
      ISSN: bookMock.ISSN,
      language: bookMock.language,
      publicationDate: bookMock.publicationDate,
      condition: bookMock.condition,
      price: bookMock.price,
      stock: bookMock.stock,
      picture: {},
    }

    const backendBook = {
      title: bookMock.title,
      author: bookMock.author,
      publicationYear: bookMock.publicationYear,
      genre: bookMock.genre,
      numberPages: bookMock.numberPages,
      editorial: bookMock.editorial,
      ISSN: bookMock.ISSN,
      language: bookMock.language,
      publicationDate: bookMock.publicationDate,
      condition: bookMock.condition,
      price: bookMock.price,
      stock: bookMock.stock,
      picture: pictureMock,
    }

    filesServiceSpy.savePicture.and.returnValue(fileMock);
    httpClientSpy.post.and.returnValue(asyncData({objectId: '1a1a1a1a1a'}));

    const returnedObject = await service.createBook(dataMock);
    
    expect(returnedObject).toEqual({objectId: '1a1a1a1a1a'});
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(APIEndpoint);
    expect(httpClientSpy.post.calls.first().args[1]).toEqual(backendBook);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
  });
});
