import { TestBed } from '@angular/core/testing';

import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { genreMock, genresMock } from 'tests/mocks/modules/genres';
import { GenreData } from '../interfaces/genre-data';
import { Genre } from '../models/genre';
import { GenresService } from './genres.service';

describe('GenresService', () => {
  let httpClientSpy: { post: jasmine.Spy, patch: jasmine.Spy, get: jasmine.Spy };
  let service: GenresService;

  const className = 'Genre';
  const APIEndpoint = API_BASE + '/classes/' + className;
  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
    service = new GenresService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected genres from server', async ()=>{
    const genresResults = {
      results: genresMock,
    };

    let returnedObjects;

    httpClientSpy.get.and.returnValue(asyncData(genresResults));

    returnedObjects = await service.getAll();

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(Genre);
      expect(element).toEqual(jasmine.objectContaining(genresMock[i]));
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(APIEndpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
  });

  it('should return a single genre with the id', async ()=>{
    const objectId = '1212121212121212121212';
    const endpoint = APIEndpoint + '/' + objectId;
    const mock = Object.assign({objectId}, genreMock) as GenreData;

    let returnedObject;

    httpClientSpy.get.and.returnValue(asyncData(mock));

    returnedObject = await service.getOne(objectId);

    expect(returnedObject).toBeInstanceOf(Genre);
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('should return user ltPreferences', async () => {
    let returnedObjects;
    let ltPreferences = [];
    let preference = {
      __type:'Pointer',
      className: 'Genre',
      objectId: '1a1a1a1a1a1a',
    };

    ltPreferences.push(preference);

    httpClientSpy.get.and.returnValue(asyncData(genreMock));

    returnedObjects = await service.getLtPreferences(ltPreferences);

    /*
    Este test no está funcionando, lo muestra como si no tuviera expects,
    mi teoría es que termina de ejecutar el test 
    antes de que la función asincrona getLtPreferences devuelva la lista
    de preferencias literarias, no sé que estoy haciendo mal :( pero se supone
    que la ejecución debe pararse en await service.getLtPreferences(...)
    hasta que getLtPreferences devuelva una lista de preferencias literarias
    ya cargadas. Como no se está deteniendo ahí el test sigue corriendo y detecta
    returnedObjects como un array vacio
    */

    returnedObjects.forEach(element => {
        expect(element).toBeInstanceOf(Genre);
        expect(element).toEqual(jasmine.objectContaining(genreMock));
    });
});

});
