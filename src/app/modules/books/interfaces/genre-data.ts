import { BackEndDataModel } from "@core/interfaces/back-end-data-model";

export interface GenreData extends BackEndDataModel {
    name: string;
}
