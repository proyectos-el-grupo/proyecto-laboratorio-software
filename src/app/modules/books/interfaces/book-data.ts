import { BackEndDataModel } from "@core/interfaces/back-end-data-model";
import { Genre } from "@core/modules/books/models/genre";
import { File } from "@core/models/file";

export interface BookData extends BackEndDataModel {
    title : string;
    author : string;
    publicationYear : number;
    genre : Genre;
    numberPages : number;
    editorial : string;
    ISSN : string;
    language : string;
    publicationDate : Date;
    condition : string;
    price : number;
    stock : number;
    picture : File
}
