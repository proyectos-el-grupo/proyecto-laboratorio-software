import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BookData } from '../../interfaces/book-data';

import { Book } from '../../models/book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss'],
})
export class BookListComponent implements OnInit {
  @Input()
  public books: Book[];
  @Output()
  public editBook: EventEmitter<BookData> = new EventEmitter();
  @Output()
  public removeBook: EventEmitter<BookData> = new EventEmitter();
  @Output()
  public bookClick: EventEmitter<BookData> = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  public edit(book: BookData) {
    this.editBook.emit(book);
  }

  public remove(book: BookData) {
    this.removeBook.emit(book);
  }

  public onClick(book: BookData) {
    this.bookClick.emit(book);
  }
}
