import { Component, Input, OnInit } from '@angular/core';
import { BookData } from '@core/modules/books/interfaces/book-data';
import { Book } from '../../models/book';

@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.scss'],
})
export class BookViewComponent implements OnInit {
  @Input()
  public book: Book;

  constructor() { }

  ngOnInit() {}

  public datePublication(): string{
    const date2 = this.book.publicationDate as any;
    const date = new Date(date2.iso);
    const anio = date.getFullYear().toString();
    const month = date.getMonth().toString();
    const day = date.getDay().toString();
    
    return day + '/' + month + '/' + anio;
  }

}
