import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { BookViewComponent } from './book-view.component';
import { bookMock } from 'tests/mocks/modules/books';

describe('BookViewComponent', () => {
  let component: BookViewComponent;
  let fixture: ComponentFixture<BookViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BookViewComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookViewComponent);
    component = fixture.componentInstance;
    component.book = bookMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the title of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.title);
  });

  it('should show the title of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.author);
  });

  it('should show the title of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.publicationDate);
  });

  it('should show the genre of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.editorial);
  });

  it('should show the title of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.numberPages);
  });

  it('should show the title of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.language);
  });

  it('should show the genre of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.stock);
  });


  it('should show the genre of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.condition);
  });

  it('should show the title of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.ISSN);
  });

  it('should show the genre of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.publicationYear);
  });

  it('should show the genre of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.genre.name);
  });


});
