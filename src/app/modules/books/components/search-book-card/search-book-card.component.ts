import { Component, Input, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Book } from '../../models/book';

@Component({
  selector: 'app-search-book-card',
  templateUrl: './search-book-card.component.html',
  styleUrls: ['./search-book-card.component.scss'],
})
export class SearchBookCardComponent implements OnInit {
  @Input()
  public book: Book;

  constructor(
    public router: Router,
  ) { }

  ngOnInit() {}

  public goToBookView(){
    let navigationExtras: NavigationExtras = {
      state: {
        book: this.book
      }
    }
    this.router.navigate(['search-book-view'], navigationExtras);
  }

}
