import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { bookMock } from 'tests/mocks/modules/books';

import { SearchBookCardComponent } from './search-book-card.component';

describe('SearchBookCardComponent', () => {
  let component: SearchBookCardComponent;
  let fixture: ComponentFixture<SearchBookCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBookCardComponent ],
      imports: [
        IonicModule.forRoot(),
        RouterTestingModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchBookCardComponent);
    component = fixture.componentInstance;
    component.book = bookMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the title of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.title);
  });

  it('should show the author of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.author);
  });

  it('should show the price of book', ()=>{
    expect(fixture.nativeElement.textContent).toContain(bookMock.price);
  });
});
