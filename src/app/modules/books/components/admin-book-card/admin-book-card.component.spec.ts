import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { bookMock } from 'tests/mocks/modules/books';

import { AdminBookCardComponent } from './admin-book-card.component';

describe('AdminBookCardComponent', () => {
  let component: AdminBookCardComponent;
  let fixture: ComponentFixture<AdminBookCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBookCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminBookCardComponent);
    component = fixture.componentInstance;
    component.book = bookMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
