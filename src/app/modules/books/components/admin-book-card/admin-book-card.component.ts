import { Component, Input, OnInit } from '@angular/core';
import { Book } from '../../models/book';

@Component({
  selector: 'app-admin-book-card',
  templateUrl: './admin-book-card.component.html',
  styleUrls: ['./admin-book-card.component.scss'],
})
export class AdminBookCardComponent implements OnInit {
  @Input()
  public book: Book;

  constructor() { }

  ngOnInit() {}

}
