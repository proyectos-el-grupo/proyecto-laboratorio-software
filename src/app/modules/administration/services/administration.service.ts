import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FrontEndModel } from '@core/models/front-end-model';
import { UserData } from '@core/modules/authentication/interfaces/user-data';
import { User } from '@core/modules/authentication/models/user';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { Person } from '@core/modules/persons/models/person';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { DataAPIService } from '@core/services/data-api.service';
import { API_BASE, API_HEADERS } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class AdministrationService extends DataAPIService<User> {
  public get className() {
    return 'users';
  }

  public modelConstructor<UserData>(data){
    return new User(data);
  }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data);
  }

  public loadAdministratorUsers(): Promise<User[]> {    
    const endpoint = API_BASE + '/' + this.className; 
    const search = {
      type: 'admin'
    };
    const object = new HttpParams().set('where', JSON.stringify(search));
    const headers = {
      headers: API_HEADERS, 
      params: object
    };

    let resultsAdmin: User;
    
    return this.httpClient.get<{results: any}>(endpoint, headers)
      .toPromise()
      .then(response => response.results)
      .then(models => {
        return models.map(model => {
          resultsAdmin = model;
          resultsAdmin = this.modelConstructor(model);

          return resultsAdmin;
        });
      })
      .catch(this.handleRequestError);
  }

  /**
   * Return all person of the array User
   * @param usersData 
   * @returns User[]
   */

  public loadPersons(usersData: User[]): User[]{
    const users: User[] = [];
    
    usersData.forEach(async element => {
      const user =  element;
      user.person = await this.personService.getOne(element.person.objectId);
      users.push(user);
    });
    return users;
  }

  public randomPassword(length: number): string{
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = ' ';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }

  public getOneUser(objectId: string): Promise<User>{
    const endpoint = API_BASE + '/' + this.className+ '/' + objectId;
    const headers = {
      headers: API_HEADERS,
    };
    let user: User;

    return this.httpClient.get<User>(endpoint, headers)
    .toPromise().then(model => {
      user = model;
      return this.personService.getOne(model.person.objectId);
    }).then(person =>{
      user.person = person;
      const backendUser = this.modelConstructor(user)
      console.log(backendUser);

      return backendUser;
    })
    .catch(this.handleRequestError);
  }

  public edit(user: User){
    const APIEndPoint = API_BASE + '/' + this.className + '/' + user.id;
    const APIHeaders = {
      headers: API_HEADERS,
    };
    let backendUser: { username: string; };

    this.personService.edit(user.person);

    backendUser = {
        username: user.username,
      };

    return this.httpClient.put<{updateAt: any}>(APIEndPoint, backendUser, APIHeaders)
    .toPromise()
    .catch(this.handleRequestError);
  }

  public deleteOneUser(id: string){
    const endpoint = API_BASE + '/' + this.className + '/' + id;
    const headers = {
      headers: {
        'X-Parse-Application-Id': 'U4noYv417O0e0O0rei1yJagQrLjQIsOVqkY6cgqX',
        'X-Parse-REST-API-Key': 'gWgVSFRNWaABLGbKXXBWAhChLIkfj59TMxbNSD3l',
        'X-Parse-Session-Token': this.authenticationService.sessionToken,
        'Content-Type': 'application/json',
      },
    };

    this.httpClient.delete(endpoint, headers);
  }

  constructor(
    protected httpClient: HttpClient,
    protected personService: PersonsService,
    protected authenticationService: AuthenticationService,
  ) {
    super(httpClient);
   }
}
