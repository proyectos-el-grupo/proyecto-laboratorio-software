import { HttpParams } from '@angular/common/http';
import { UserData } from '@core/modules/authentication/interfaces/user-data';
import { User } from '@core/modules/authentication/models/user';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { personMock } from 'tests/mocks/modules/persons';

import { AdministrationService } from './administration.service';

describe('AdministrationService', () => {
  let httpClientSpy: { 
    post: jasmine.Spy, 
    put: jasmine.Spy, 
    get: jasmine.Spy,
    delete: jasmine.Spy
  };
  let personsServiceSpy: {
    edit: jasmine.Spy,
    getOne: jasmine.Spy,
  };
  let authenticationServiceSpy: {
    sessionToken: jasmine.Spy,
  };
  let service: AdministrationService;

  const className = 'users';
  const APIEndpoint = API_BASE + '/' + className;
  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'post', 'delete']);
    personsServiceSpy = jasmine.createSpyObj('PersonsService', ['edit', 'getOne']);
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', ['sessionToken']);
    service = new AdministrationService(
      httpClientSpy as any, 
      personsServiceSpy as any, 
      authenticationServiceSpy as any
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a single object with the id', async ()=>{
    const objectId = '1212121212121212121212';
    const endpoint = APIEndpoint + '/' + objectId;
    const userAdministratorMock = {
      username: 'some@email.com',
      password: 'somePassword123',
      type: 'admin',
      person: personMock
    }as UserData; 
    const mock = Object.assign({objectId},userAdministratorMock) as UserData;

    let returnedObject;

    httpClientSpy.get.and.returnValue(asyncData(mock));

    returnedObject = await service.getOneUser(objectId);

    expect(returnedObject).toBeInstanceOf(User);
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('should result all administrators user', async()=> {
    const endpoint = APIEndpoint;
    const userAdministratorMock = [
      {
        username: 'some@email.com',
        type: 'admin',
        person: personMock
      },
      {
        username: 'some@email.com',
        type: 'admin',
        person: personMock
      },
      {
        username: 'some@email.com',
        type: 'admin',
        person: personMock
      }
    ]as any as UserData;

    const search = {
      type: 'admin'
    };

    const object = new HttpParams().set('where', JSON.stringify(search));
    const headers = {
      headers: APIHeaders,
      params: object
    };

    const userAdministratorResults = {
      results: userAdministratorMock
    };

    let returnedObjects;
    
    httpClientSpy.get.and.returnValue(asyncData(userAdministratorResults));
    personsServiceSpy.getOne.and.returnValue(Promise.resolve(personMock));

    returnedObjects = await service.loadAdministratorUsers();

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(User);
      expect(element).toEqual(jasmine.objectContaining(userAdministratorMock[i]));
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual(headers);
  });

  it('should create a random password', ()=>{
    const random = service.randomPassword(10);
    const random2 = service.randomPassword(10);
    const random3 = service.randomPassword(10);

    expect(random === random2).toBeFalsy();
    expect(random === random3).toBeFalsy();
    expect(random2 === random3).toBeFalsy();
  });

  it('should edit administrator user', async()=>{
    const objectId = '1212121212121212121212';
    const endpoint = APIEndpoint + '/' + objectId;
    const dateUser = new Date(1999,1,2);
    const userAdministratorMock = {
      username: 'some@email.com',
      password: 'somePassword123',
      type: 'admin',
      person: personMock
    }as User; 
    const userMock = Object.assign({id: objectId}, userAdministratorMock) as User;

    const backendUserMock = {
      username: userMock.username,
    };

    httpClientSpy.put.and.returnValue(asyncData({updateAt: dateUser}));

    const returnedObject = await service.edit(userMock);

    expect(returnedObject).toEqual({updateAt: dateUser});
    expect(httpClientSpy.put).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.put.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.put.calls.first().args[1]).toEqual(backendUserMock);
    expect(httpClientSpy.put.calls.first().args[2]).toEqual({headers: APIHeaders});
  });

  it('should delete a specific user', async ()=>{

    authenticationServiceSpy.sessionToken.and.returnValue('r:pnktnjyb996sj4p156gjtp4im');

    const id = '1212121212';
    const endpoint = APIEndpoint + '/' + id;
    const sessionToken = authenticationServiceSpy.sessionToken;
    const APIHeaders = {
      headers: {
        'X-Parse-Application-Id': 'U4noYv417O0e0O0rei1yJagQrLjQIsOVqkY6cgqX',
        'X-Parse-REST-API-Key': 'gWgVSFRNWaABLGbKXXBWAhChLIkfj59TMxbNSD3l',
        'X-Parse-Session-Token': sessionToken,
        'Content-Type': 'application/json',
      }
    };

    await service.deleteOneUser(id);

    expect(httpClientSpy.delete).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.delete.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.delete.calls.first().args[1]).toEqual(APIHeaders);
  });
});
