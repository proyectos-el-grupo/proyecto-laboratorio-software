import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdministrationFormPageRoutingModule } from './administration-form-routing.module';

import { AdministrationFormPage } from './administration-form.page';
import { AdministrationFormComponent } from '../../components/administration-form/administration-form/administration-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdministrationFormPageRoutingModule,
    ReactiveFormsModule,
    ],
  declarations: [
  ]
})
export class AdministrationFormPageModule {}
