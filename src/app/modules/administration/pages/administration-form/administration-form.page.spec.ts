import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { SignupService } from '@core/modules/authentication/services/signup.service';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { IonicModule, ModalController } from '@ionic/angular';
import { AdministrationFormComponent } from '../../components/administration-form/administration-form/administration-form.component';

import { AdministrationFormPage } from './administration-form.page';

describe('AdministrationFormPage', () => {
  let component: AdministrationFormPage;
  let fixture: ComponentFixture<AdministrationFormPage>;
  let administrationServiceSpy: {
    modelConstructor: jasmine.Spy,
  };
  let signupServiceSpy: {
    singUp: jasmine.Spy,
  };
  let personsServiceSpy: {
    modelConstructor: jasmine.Spy,
  }

  beforeEach(waitForAsync(() => {
    administrationServiceSpy = jasmine.createSpyObj('AdministrationService', ['modelConstructor']);
    signupServiceSpy = jasmine.createSpyObj('SingupService', ['singUp']);
    personsServiceSpy = jasmine.createSpyObj('PersonsService', ['modelConstructor']);
    TestBed.configureTestingModule({
      declarations: [ 
        AdministrationFormPage,
        AdministrationFormComponent,
      ],
      providers: [{
        provide: AuthenticationService,
        useValue: administrationServiceSpy,
        },
        {
          provide: SignupService,
          useValue: signupServiceSpy,
        },
        {
          provide: PersonsService,
          useValue: personsServiceSpy,
        },
        {
          provide: ModalController
        },
        {
          provide: FormBuilder,
        }
      ],
      imports: [
        HttpClientModule,
        ReactiveFormsModule,
        IonicModule.forRoot()
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AdministrationFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
