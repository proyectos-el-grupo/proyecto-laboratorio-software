import { Component, OnInit } from '@angular/core';
import { UserData } from '@core/modules/authentication/interfaces/user-data';
import { User } from '@core/modules/authentication/models/user';
import { SignupService } from '@core/modules/authentication/services/signup.service';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { ModalController } from '@ionic/angular';
import { AdministrationService } from '../../services/administration.service';

@Component({
  selector: 'app-administration-form-page',
  templateUrl: './administration-form.page.html',
  styleUrls: ['./administration-form.page.scss'],
})
export class AdministrationFormPage implements OnInit {
  public userData: User;

  constructor(
    public modalController: ModalController,
    private administrationService: AdministrationService,
    private signupService: SignupService,
    private personsService: PersonsService,
  ) { }

  ngOnInit() {
  }

  public onSubmit(userData: any) {
    let user: User;
    if(!this.userData){
      user = {
        username: userData.username,
        type: 'admin',
        person: {
          name: userData.name,
          lastname: userData.lastname,
          dni: userData.dni,
        },
      }as User;
    } else {
      user = {
        id: userData.id,
        username: userData.username,
        type: userData.type,
        person: {
          id: userData.personId,
          name: userData.name,
          lastname: userData.lastname,
          dni: userData.dni,
        },
      }as User;
    }
    this.save(user);
  }

  public async save(userData: User){
    if(!this.userData){
      const person = this.personsService.modelConstructor(userData.person);
      const user = this.administrationService.modelConstructor(userData);
      user.person = person;
    
      await this.signupService.signUp(user);
    } else {
      await this.administrationService.edit(userData);
    }

    this.onAfterSaveData();
  }

  public onAfterSaveData(){
    
    this.modalController.dismiss();
  }

  public onCancelForm(){
    this.modalController.dismiss();
  }

}
