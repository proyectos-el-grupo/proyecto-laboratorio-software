import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdministrationFormPage } from './administration-form.page';

const routes: Routes = [
  {
    path: '',
    component: AdministrationFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationFormPageRoutingModule {}
