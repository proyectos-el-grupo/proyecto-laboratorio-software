import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonalDataFormPage } from './personal-data-form.page';

const routes: Routes = [
  {
    path: '',
    component: PersonalDataFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonalDataFormPageRoutingModule {}
