import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { IonicModule } from '@ionic/angular';
import { AdministrationService } from '../../services/administration.service';

import { PersonalDataFormPage } from './personal-data-form.page';

describe('PersonalDataFormPage', () => {
  let component: PersonalDataFormPage;
  let fixture: ComponentFixture<PersonalDataFormPage>;
  let administrationServiceSpy: {
    edit: jasmine.Spy,
  };

  beforeEach(waitForAsync(() => {
    administrationServiceSpy = jasmine.createSpyObj('AdministrationService', ['edit']);
    TestBed.configureTestingModule({
      declarations: [ 
        PersonalDataFormPage,
      ],
      imports: [
        IonicModule.forRoot(),
        HttpClientModule,
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: AuthenticationService,
          useValue: administrationServiceSpy,
        },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PersonalDataFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
