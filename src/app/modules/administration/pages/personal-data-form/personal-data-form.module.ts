import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonalDataFormPageRoutingModule } from './personal-data-form-routing.module';

import { PersonalDataFormPage } from './personal-data-form.page';
import { PersonalDataFormComponent } from '../../components/personal-data-form/personal-data-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonalDataFormPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    PersonalDataFormPage,
    PersonalDataFormComponent,
  ]
})
export class PersonalDataFormPageModule {}
