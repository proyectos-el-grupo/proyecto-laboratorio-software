import { Component, OnInit } from '@angular/core';
import { UserData } from '@core/modules/authentication/interfaces/user-data';
import { User } from '@core/modules/authentication/models/user';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { ModalController, ToastController } from '@ionic/angular';
import { AdministrationService } from '../../services/administration.service';

@Component({
  selector: 'app-personal-data-form-page',
  templateUrl: './personal-data-form.page.html',
  styleUrls: ['./personal-data-form.page.scss'],
})
export class PersonalDataFormPage implements OnInit {
  public user: User;

  constructor(
    public modalController: ModalController,
    protected administrationService: AdministrationService,
    protected personsService: PersonsService,
    protected toastController: ToastController,
  ) { }

  ngOnInit() {
  }

  public async onSubmit(userData: any) {
    let user = {
      id: userData.id,
      username: userData.username,
      type: userData.type,
      person: {
        id: userData.personId,
        name: userData.name,
        lastname: userData.lastname,
        dni: userData.dni,
        birthdate: userData.birthdate,
        birthplace: userData.birthplace,
        address: userData.address,
        city: userData.city,
        gender: userData.gender,
      },
    }as User;
    this.save(user);
  }

  public async save(userData: User){
    try {
      await this.administrationService.edit(userData);
    } catch (error) {
      this.handleHttpError(error);
    }

    this.onAfterSave();
  }

  public onAfterSave(){
    this.modalController.dismiss();
  }

  public onCancelForm(){
    this.modalController.dismiss();
  }

  public async handleHttpError(errorResponse) {
    let message: string;

    if (errorResponse.error.error){
      message = errorResponse.error.error;
    } else {
      message = 'Cannot connect to server';
    }

    const toast = await this.toastController.create({
      message,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }
}
