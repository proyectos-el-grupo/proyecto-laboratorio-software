import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { IonicModule } from '@ionic/angular';
import { asyncData } from 'tests/helpers';
import { personMock } from 'tests/mocks/modules/persons';
import { userAdministratorsMock } from 'tests/mocks/modules/user-administrator';
import { AdministrationCardComponent } from '../../components/administration-card /administration-card.component';
import { AdministrationFormComponent } from '../../components/administration-form/administration-form/administration-form.component';
import { AdministrationListComponent } from '../../components/administration-list/administration-list.component';
import { AdministrationService } from '../../services/administration.service';

import { AdministrationListPage } from './administration-list.page';

describe('AdministrationListPage', () => {
  let component: AdministrationListPage;
  let fixture: ComponentFixture<AdministrationListPage>;
  let administrationServiceSpy: {
    loadAdministratorUsers: jasmine.Spy,
    loadPersons: jasmine.Spy,
    deleteOne: jasmine.Spy,
  };
  let personsServiceSpy: {
    getOne: jasmine.Spy,
  }

  beforeEach(waitForAsync(() => {
    administrationServiceSpy = jasmine.createSpyObj('AdministrationService', [
                                                   'loadAdministratorUsers',
                                                    'loadPersons',
                                                   'deleteOne'
                                                  ]);
    personsServiceSpy = jasmine.createSpyObj('PersonsService', [
                                                    'getOne',
                                                   ]);
    administrationServiceSpy.loadAdministratorUsers.and.returnValue(Promise.resolve(userAdministratorsMock));
    administrationServiceSpy.loadPersons.and.returnValue(userAdministratorsMock);
    personsServiceSpy.getOne.and.returnValue(Promise.resolve(personMock));
    TestBed.configureTestingModule({
      declarations: [ 
        AdministrationListPage,
        AdministrationFormComponent,
        AdministrationListComponent,
        AdministrationCardComponent,
      ],
      providers: [
        {
          provide: AdministrationService,
          useValue: administrationServiceSpy,
        },
        {
          provide: PersonsService,
          useValue: personsServiceSpy
        },
      ],
      imports: [
        HttpClientModule,
        RouterTestingModule,
        IonicModule.forRoot()
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AdministrationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the same amount of `User`s it loads', ()=>{
    fixture.detectChanges();

    const cards = fixture.nativeElement.querySelectorAll('app-administration-card');

    expect(cards.length).toEqual(userAdministratorsMock.length);
  });
});
