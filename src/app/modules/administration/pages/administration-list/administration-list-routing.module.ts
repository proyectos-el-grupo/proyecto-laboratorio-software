import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdministrationListPage } from './administration-list.page';

const routes: Routes = [
  {
    path: '',
    component: AdministrationListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationListPageRoutingModule {}
