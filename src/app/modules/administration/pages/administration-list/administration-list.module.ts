import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdministrationListPageRoutingModule } from './administration-list-routing.module';

import { AdministrationListPage } from './administration-list.page';
import { AdministrationListComponent } from '../../components/administration-list/administration-list.component';
import { AdministrationCardComponent } from '../../components/administration-card /administration-card.component';
import { AdministrationFormComponent } from '../../components/administration-form/administration-form/administration-form.component';
import { AdministrationFormPage } from '../administration-form/administration-form.page';
import { PersonalDataFormPage } from '../personal-data-form/personal-data-form.page';
import { PersonalDataFormComponent } from '../../components/personal-data-form/personal-data-form.component';
import { SignupFormComponent } from '@core/modules/authentication/components/signup-form/signup-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdministrationListPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    AdministrationListPage,
    AdministrationCardComponent,
    AdministrationListComponent,
    AdministrationFormComponent,
    AdministrationFormPage,
  ],
  providers: [
    SignupFormComponent,
  ]
})
export class AdministrationListPageModule {}
