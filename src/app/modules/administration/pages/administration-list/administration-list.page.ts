import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserData } from '@core/modules/authentication/interfaces/user-data';
import { User } from '@core/modules/authentication/models/user';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { LoginService } from '@core/modules/authentication/services/login.service';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { ModalController } from '@ionic/angular';
import { AdministrationService } from '../../services/administration.service';
import { AdministrationFormPage } from '../administration-form/administration-form.page';
import { PersonalDataFormPage } from '../personal-data-form/personal-data-form.page';

@Component({
  selector: 'app-administration-list-page',
  templateUrl: './administration-list.page.html',
  styleUrls: ['./administration-list.page.scss'],
})
export class AdministrationListPage implements OnInit {
  public administrators: User[];

  constructor(
    public modalController: ModalController,
    private authenticationService: AuthenticationService,
    private administrationService: AdministrationService,
    private personsService: PersonsService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.verifySession();
    this.loadAdministrators();
  }

  public verifySession(){
    if(!this.authenticationService.isAuthenticated &&
       this.authenticationService.userType !== 'root'){
      this.router.navigateByUrl('/search-book', {skipLocationChange: false});
    }
  }

  public async loadAdministrators(){
   const some = await this.administrationService.loadAdministratorUsers();
   this.administrators = this.administrationService.loadPersons(some);
  }

  public gotToCreateAdministrator(){
    this.goToAdministratorForm();
  }

  public onEditAdministrator(userData: UserData) {
    this.goToAdministratorForm(userData);
  }

  public onRemoveAdministrator(userData){
    this.personsService.deleteOne(userData.person.id);
    this.administrationService.deleteOneUser(userData.id);
    this.loadAdministrators();
  }

  public async goToAdministratorForm(userData: UserData = null) {
    const modal = await this.modalController.create({
      component: AdministrationFormPage,
      componentProps: {
        userData: userData
      },
    });
    await modal.present();
  }
}
