import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '@core/modules/authentication/models/user';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-administration-form',
  templateUrl: './administration-form.component.html',
  styleUrls: ['./administration-form.component.scss'],
})
export class AdministrationFormComponent implements OnInit {
  @Input()
  public userData: User;
  @Output()
  public submitForm: EventEmitter<{username: string, 
                                    name: string, 
                                    lastname: string, 
                                    dni: string,
                                  }> = new EventEmitter();
  @Output()
  public cancelForm: EventEmitter<void> = new EventEmitter();

  public administrationForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastController: ToastController,
  ) { 
    this.administrationForm = this.formBuilder.group({
      username: ['', [Validators.minLength(5), Validators.email, Validators.required]],
      name: ['', [Validators.maxLength(24),Validators.required]],
      lastname: ['', [Validators.maxLength(24), Validators.required]],
      dni: ['', [Validators.minLength(8), 
                Validators.maxLength(10), 
                Validators.pattern('^[0-9,$]*$'),
                Validators.required,]
            ]
    });
  }

  ngOnInit() {
    this.fillInForm();
  }

  public fillInForm(){
    if(this.userData){
      this.administrationForm.setValue({
        username: this.userData.username,
        name: this.userData.person.name,
        lastname: this.userData.person.lastname,
        dni: this.userData.person.dni,
      });

      this.administrationForm.disable();
    }
  }

  public async onSubmit(): Promise<void> {
    if(!this.administrationForm.valid) {
      if(this.administrationForm.controls.username.hasError('email')) {
        await this.handleUsernameMisspelled();
        return;
      }
      if(this.administrationForm.controls.name.hasError('required')) {
        await this.handleNameOmission();
        return;
      }
      if(this.administrationForm.controls.lastname.hasError('required')) {
        await this.handleLastnameOmission();
        return;
      }
      if(this.administrationForm.controls.dni.hasError('required')) {
        await this.handleDniOmission();
        return;
      }else if(this.administrationForm.controls.dni.hasError('pattern')){
        await this.handleDniMisspelled();
        return;
      }
    }
    if(!this.userData){
      const value = this.administrationForm.value;

      this.submitForm.emit(value);
    } else {
      let value = this.administrationForm.value;
      const valueEmit = Object.assign({
        id: this.userData.id,
        personId: this.userData.person.id,
        type: this.userData.type,
      }, value);

      this.submitForm.emit(valueEmit);
    }   
  }
  public onCancel(): void{
    this.cancelForm.emit();
  }

  public enableForm(){
    this.administrationForm.enable();
  }

  public async handleUsernameMisspelled() {
    const toast = await this.toastController.create({
      message: 'El nombre de usuario debe ser un correo',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handleNameOmission() {
    const toast = await this.toastController.create({
      message: 'Debe proporcionar un nombre',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handleLastnameOmission() {
    const toast = await this.toastController.create({
      message: 'Debe proporcionar un apellido',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handleDniOmission() {
    const toast = await this.toastController.create({
      message: 'Debee proporcionar un número de documento',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handleDniMisspelled() {
    const toast = await this.toastController.create({
      message: 'El número de documento debe tener solo números',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }
}
