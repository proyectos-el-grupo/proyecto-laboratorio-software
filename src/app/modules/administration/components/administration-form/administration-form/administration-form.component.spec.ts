import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { AdministrationFormComponent } from './administration-form.component';

describe('AdministrationFormComponent', () => {
  let component: AdministrationFormComponent;
  let fixture: ComponentFixture<AdministrationFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationFormComponent ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AdministrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be false when form is empty', ()=>{
    expect(component.administrationForm.valid).toBeFalsy();
  });

  it('should be true when username is not an email', ()=>{
    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('somename');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('0123456789');

    expect(component.administrationForm.valid).toBeFalsy();
  });

  it('should be true when username is an email', ()=>{
    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('0123456789');

    expect(component.administrationForm.valid).toBeTruthy();
  });

  it('should be false when username is less 5 characters', ()=>{
    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('@e.');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('0123456789');

    expect(component.administrationForm.valid).toBeFalsy();
  });

  it('should be false when name is over 24 characters', ()=>{
    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('somenameaaaaaaaaaaaaaaaaa');
    lastname.setValue('somelastname');
    dni.setValue('0123456789');

    expect(component.administrationForm.valid).toBeFalsy();
  });

  it('should be false when lastname is over 24 characters', ()=>{
    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('somename');
    lastname.setValue('somelastnameaaaaaaaaaaaaa');
    dni.setValue('0123456789');

    expect(component.administrationForm.valid).toBeFalsy();
  });

  it('should be false when write some character different of number', ()=>{
    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('012345678p');

    expect(component.administrationForm.valid).toBeFalsy();
  });

  it('should emit administration information when submit button is clicked', ()=>{
    const button = fixture.nativeElement.querySelector('ion-button');
    const administrationFormEmmiter = spyOn(component.submitForm, 'emit');

    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    const administrationFormMock = {
      username: 'some@email.com',
      name: 'somename',
      lastname: 'somelastname',
      dni: '0123456789'
    };

    username.setValue('some@email.com');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('0123456789');

    button.click();

    expect(administrationFormEmmiter).toHaveBeenCalledTimes(1);
    expect(administrationFormEmmiter).toHaveBeenCalledWith(administrationFormMock);
  });

  it('should show a toast when the username is not a email', ()=>{
    const handleUsernameMisspelledSpy = spyOn(component, 'handleUsernameMisspelled');
    const handleNameOmission = spyOn(component, 'handleNameOmission');
    const handleLastnameOmission = spyOn(component, 'handleLastnameOmission');
    const handleDniOmission = spyOn(component, 'handleDniOmission');
    const handleDniMisspelled = spyOn(component, 'handleDniMisspelled');

    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('somename');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('0123456789');

    button.click();

    expect(component.administrationForm.valid).toBeFalsy();
    expect(handleUsernameMisspelledSpy).toHaveBeenCalledTimes(1);
    expect(handleNameOmission).toHaveBeenCalledTimes(0);
    expect(handleLastnameOmission).toHaveBeenCalledTimes(0);
    expect(handleDniMisspelled).toHaveBeenCalledTimes(0);
    expect(handleDniOmission).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when the name is omitted', ()=>{
    const handleUsernameMisspelledSpy = spyOn(component, 'handleUsernameMisspelled');
    const handleNameOmission = spyOn(component, 'handleNameOmission');
    const handleLastnameOmission = spyOn(component, 'handleLastnameOmission');
    const handleDniOmission = spyOn(component, 'handleDniOmission');
    const handleDniMisspelled = spyOn(component, 'handleDniMisspelled');

    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('');
    lastname.setValue('somelastname');
    dni.setValue('0123456789');

    button.click();

    expect(component.administrationForm.valid).toBeFalsy();
    expect(handleNameOmission).toHaveBeenCalledTimes(1);
    expect(handleUsernameMisspelledSpy).toHaveBeenCalledTimes(0);
    expect(handleLastnameOmission).toHaveBeenCalledTimes(0);
    expect(handleDniMisspelled).toHaveBeenCalledTimes(0);
    expect(handleDniOmission).toHaveBeenCalledTimes(0);
  });
  
  it('should show a toast when the lastname is omitted', ()=>{
    const handleUsernameMisspelledSpy = spyOn(component, 'handleUsernameMisspelled');
    const handleNameOmission = spyOn(component, 'handleNameOmission');
    const handleLastnameOmission = spyOn(component, 'handleLastnameOmission');
    const handleDniOmission = spyOn(component, 'handleDniOmission');
    const handleDniMisspelled = spyOn(component, 'handleDniMisspelled');

    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('somename');
    lastname.setValue('');
    dni.setValue('0123456789');

    button.click();

    expect(component.administrationForm.valid).toBeFalsy();
    expect(handleLastnameOmission).toHaveBeenCalledTimes(1);
    expect(handleUsernameMisspelledSpy).toHaveBeenCalledTimes(0);
    expect(handleNameOmission).toHaveBeenCalledTimes(0);
    expect(handleDniMisspelled).toHaveBeenCalledTimes(0);
    expect(handleDniOmission).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when the dni is omitted', ()=>{
    const handleUsernameMisspelledSpy = spyOn(component, 'handleUsernameMisspelled');
    const handleNameOmission = spyOn(component, 'handleNameOmission');
    const handleLastnameOmission = spyOn(component, 'handleLastnameOmission');
    const handleDniOmission = spyOn(component, 'handleDniOmission');
    const handleDniMisspelled = spyOn(component, 'handleDniMisspelled');

    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('');

    button.click();

    expect(component.administrationForm.valid).toBeFalsy();
    expect(handleDniOmission).toHaveBeenCalledTimes(1);
    expect(handleUsernameMisspelledSpy).toHaveBeenCalledTimes(0);
    expect(handleNameOmission).toHaveBeenCalledTimes(0);
    expect(handleLastnameOmission).toHaveBeenCalledTimes(0);
    expect(handleDniMisspelled).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when the dni is misspelled', ()=>{
    const handleUsernameMisspelledSpy = spyOn(component, 'handleUsernameMisspelled');
    const handleNameOmission = spyOn(component, 'handleNameOmission');
    const handleLastnameOmission = spyOn(component, 'handleLastnameOmission');
    const handleDniOmission = spyOn(component, 'handleDniOmission');
    const handleDniMisspelled = spyOn(component, 'handleDniMisspelled');

    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.administrationForm.controls.username;
    const name = component.administrationForm.controls.name;
    const lastname = component.administrationForm.controls.lastname;
    const dni = component.administrationForm.controls.dni;

    username.setValue('some@email.com');
    name.setValue('somename');
    lastname.setValue('somelastname');
    dni.setValue('012345678p');

    button.click();

    expect(component.administrationForm.valid).toBeFalsy();
    expect(handleDniMisspelled).toHaveBeenCalledTimes(1);
    expect(handleUsernameMisspelledSpy).toHaveBeenCalledTimes(0);
    expect(handleNameOmission).toHaveBeenCalledTimes(0);
    expect(handleLastnameOmission).toHaveBeenCalledTimes(0);
    expect(handleDniOmission).toHaveBeenCalledTimes(0);
  });
});
