import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { SignupFormComponent } from '@core/modules/authentication/components/signup-form/signup-form.component';
import { User } from '@core/modules/authentication/models/user';
import { PlacesService } from '@core/services/places.service';
import { IonicModule, ModalController, ToastController } from '@ionic/angular';
import { genresMock } from 'tests/mocks/modules/genres';
import { personMock, personsMock } from 'tests/mocks/modules/persons';
import { placeMock, placesMock } from 'tests/mocks/modules/places';
import { userAdministratorMock } from 'tests/mocks/modules/user-administrator';
import { AdministrationFormComponent } from '../administration-form/administration-form/administration-form.component';

import { PersonalDataFormComponent } from './personal-data-form.component';

describe('PersonalDataFormComponent', () => {
  let component: PersonalDataFormComponent;
  let fixture: ComponentFixture<PersonalDataFormComponent>;
  let personsServiceSpy: {
    getAll: jasmine.Spy,
    getOne: jasmine.Spy,
  };

  beforeEach(waitForAsync(() => {
    personsServiceSpy = jasmine.createSpyObj('PërsonsService', ['getAll', 'getOne']);
    personsServiceSpy.getAll.and.returnValue(Promise.resolve(placesMock));
    personsServiceSpy.getOne.and.returnValue(Promise.resolve(placeMock));
    TestBed.configureTestingModule({
      declarations: [ 
        PersonalDataFormComponent,
      ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        HttpClientModule
      ],
      providers: [
        AdministrationFormComponent,
        {
          provide: PlacesService,
          useValue: personsServiceSpy,
        },
        ToastController,
        ModalController,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PersonalDataFormComponent);
    component = fixture.componentInstance;
    component.userData = {
      username: 'some@email.com',
      type: 'client',
      person: personMock,
    }as User;
    component.places = placesMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be true when the user it is a client', ()=>{
    const formMock = {
      username: userAdministratorMock.username,
      name: userAdministratorMock.person.name,
      lastname: userAdministratorMock.person.lastname,
      dni: userAdministratorMock.person.dni,
      birthdate: userAdministratorMock.person.birthdate,
      birthState: userAdministratorMock.person.birthplace.state,
      birthplace: userAdministratorMock.person.birthplace,
      address: userAdministratorMock.person.address,
      state: userAdministratorMock.person.city.state,
      city: userAdministratorMock.person.city,
      gender: userAdministratorMock.person.gender,
    }; 
    
    expect(component.clientForm.value).toEqual(formMock);
  });

  it('should emit the information with the new data', ()=>{
    const formMock = {
      username: userAdministratorMock.username,
      name: 'newName',
      lastname: userAdministratorMock.person.lastname,
      dni: userAdministratorMock.person.dni,
      birthdate: userAdministratorMock.person.birthdate,
      birthState: userAdministratorMock.person.birthplace.state,
      birthplace: userAdministratorMock.person.birthplace,
      address: userAdministratorMock.person.address,
      state: userAdministratorMock.person.city.state,
      city: userAdministratorMock.person.city,
      gender: userAdministratorMock.person.gender,
    };

    const name = component.clientForm.controls.name;
    name.setValue('newName');

    const button = fixture.nativeElement.querySelector('#emitClient');
    const submitForm = spyOn(component, 'onSubmit');

    button.click();

    expect(submitForm).toHaveBeenCalledTimes(1);
    expect(component.clientForm.value).toEqual(formMock);
  });
});
