import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PlaceData } from '@core/interfaces/place-data';
import { Place } from '@core/models/place';
import { User } from '@core/modules/authentication/models/user';
import { PlacesService } from '@core/services/places.service';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-personal-data-form',
  templateUrl: './personal-data-form.component.html',
  styleUrls: ['./personal-data-form.component.scss'],
})
export class PersonalDataFormComponent implements OnInit {
  @Input()
  public userData: User;

  @Output()
  public submitForm: EventEmitter<{
    id: string,
    username: string,
    personid: string,
    name: string,
    lastname: string,
    dni: string,
    birthdate: Date,
    birthplace: Place,
    address: string,
    city: Place,
    gender: string,
  }> = new EventEmitter();
  @Output()
  public cancelForm: EventEmitter<void> = new EventEmitter();

  public states;
  public formBirthplaces;
  public cities;
  public clientForm: FormGroup;
  public places: PlaceData[];
  public birthplaceFromService: Place;
  public cityFromService: Place;

  constructor(
    private formBuilder: FormBuilder,
    private placesService: PlacesService,
    public modalController: ModalController,
    public toastController: ToastController,
  ) {
     
    this.clientForm = this.formBuilder.group({
      username: ['', [
        Validators.minLength(5),
        Validators.maxLength(60),
        Validators.email,
        Validators.required
      ]],
      name: ['', [
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.required
      ]],
      lastname: ['', [
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.required
      ]],
      dni: ['', [Validators.minLength(6), 
        Validators.maxLength(10), 
        Validators.pattern('^[0-9,$]*$'),
        Validators.required,]
      ],
      birthdate: ['', Validators.required],
      birthState: [''],
      birthplace: ['', Validators.required],
      address: ['', [
        Validators.minLength(6),
        Validators.maxLength(64),
        Validators.required
      ]],
      state: [''],
      city: ['', Validators.required],
      gender: ['', Validators.required],
    });
  }

  async ngOnInit() {
    this.places = await this.placesService.getAll();
    this.states = this.getStates();
    this.fillInForm();
  }

  public async fillInForm(){
    this.birthplaceFromService = await this.placesService.getOne(this.userData.person.birthplace.objectId);
    this.cityFromService = await this.placesService.getOne(this.userData.person.city.objectId);
    if(this.userData){
      this.clientForm.setValue({
        username: this.userData.username,
        name: this.userData.person.name,
        lastname: this.userData.person.lastname,
        dni: this.userData.person.dni,
        gender: this.userData.person.gender,
        birthdate: this.userData.person.birthdate,
        birthState: this.birthplaceFromService.state,
        birthplace: this.birthplaceFromService,
        address: this.userData.person.address,
        state: this.cityFromService.state,
        city: this.cityFromService,
      });

      this.clientForm.disable();
    }
  }

  public enableForm(){
    this.clientForm.enable();
  }

  public async onSubmit(){    
    if (!this.clientForm.valid){
      this.invalidFormToast();
      return;
    }
  
    let value = this.clientForm.value;
    const valueEmit = Object.assign({
      id: this.userData.id,
      personId: this.userData.person.id,
      type: this.userData.type,
    }, value);
    console.log('value', value);
    console.log('valueEmit', valueEmit);

    this.submitForm.emit(valueEmit);
  }
  
  public onCancel(): void{
    this.cancelForm.emit();
  }

  public getBirthplaces(state: any): void{
    this.formBirthplaces=this.places.filter(city => city.state == state.detail.value);
  }

  public getCities(state: any): void{
    this.cities=this.places.filter(city => city.state == state.detail.value);    
  }

  private getStates(){
    let states = [];
    this.places.forEach((element) => {
        if (!states.includes(element.state)){
          states.push(element.state);
        }
      });
    states.sort();
    console.log(states);
    
    return states;
  }

  public async invalidFormToast() {
    const toast = await this.toastController.create({
      message: 'Faltan campos o la información es incorrecta',
      duration: 2000,
    });
    toast.present();
  }
}
