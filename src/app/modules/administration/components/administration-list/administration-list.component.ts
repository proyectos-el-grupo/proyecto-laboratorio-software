import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserData } from '@core/modules/authentication/interfaces/user-data';

@Component({
  selector: 'app-administration-list',
  templateUrl: './administration-list.component.html',
  styleUrls: ['./administration-list.component.scss'],
})
export class AdministrationListComponent implements OnInit {
  @Input()
  public administrators: UserData[];
  @Output()
  public editClick: EventEmitter<UserData> = new EventEmitter();
  @Output()
  public removeClick: EventEmitter<UserData> = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  public edit(administrator: UserData) {
    this.editClick.emit(administrator);
  }

  public delete(administrator: UserData) {
    this.removeClick.emit(administrator);
  }

}
