import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { userAdministratorMock, userAdministratorsMock } from 'tests/mocks/modules/user-administrator';

import { AdministrationListComponent } from './administration-list.component';

describe('AdministrationListComponent', () => {
  let component: AdministrationListComponent;
  let fixture: ComponentFixture<AdministrationListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationListComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdministrationListComponent);
    component = fixture.componentInstance;
    component.administrators = userAdministratorsMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show cards of administrators', ()=> {
    const administratorList = fixture.nativeElement.querySelectorAll('app-administration-card');

    expect(administratorList.length).toBe(userAdministratorsMock.length);
  });

  it('should show edit button of every card', ()=> {
    const administratorList = fixture.nativeElement.querySelectorAll('#edit');

    expect(administratorList.length).toBe(userAdministratorsMock.length)
  });

  it('should show delete button of every card', ()=> {
    const administratorList = fixture.nativeElement.querySelectorAll('#deleteButton');

    expect(administratorList.length).toBe(userAdministratorsMock.length)
  });

  it('should emit a modal when the edit button is clicked', ()=> {
    const buttonEdit = fixture.nativeElement.querySelector('#edit');
    const editEmmiter = spyOn(component.editClick, 'emit');

    buttonEdit.click();

    expect(editEmmiter).toHaveBeenCalledTimes(1);
    expect(editEmmiter).toHaveBeenCalledWith(userAdministratorMock);
  });

  it('should emit a modal when the delete button is clicked', ()=>{
    const buttonDelete = fixture.nativeElement.querySelector('#deleteButton');
    const eliminateEmmiter = spyOn(component.removeClick, 'emit');

    buttonDelete.click();

    expect(eliminateEmmiter).toHaveBeenCalledTimes(1);
    expect(eliminateEmmiter).toHaveBeenCalledWith(userAdministratorMock);
  });
});
