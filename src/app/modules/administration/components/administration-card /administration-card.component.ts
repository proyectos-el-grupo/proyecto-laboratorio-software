import { Component, Input, OnInit } from '@angular/core';
import { UserData } from '@core/modules/authentication/interfaces/user-data';

@Component({
  selector: 'app-administration-card',
  templateUrl: './administration-card.component.html',
  styleUrls: ['./administration-card.component.scss'],
})
export class AdministrationCardComponent implements OnInit {
  @Input()
  public administration: UserData;

  constructor() { }

  ngOnInit() {}

  public fullname(): string{
    return this.administration.person.name + ' ' + this.administration.person.lastname;
  }
}
