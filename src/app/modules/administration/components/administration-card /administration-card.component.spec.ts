import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdministrationCardComponent } from './administration-card.component';
import { UserData } from '@core/modules/authentication/interfaces/user-data';
import { PersonData } from '@core/modules/persons/interfaces/person-data';

describe('AdministrationCardComponent', () => {
  let component: AdministrationCardComponent;
  let fixture: ComponentFixture<AdministrationCardComponent>;

  const personMock = {
    dni: '0123456789',
    name: 'somename',
    lastname: 'somelastname'
  }as PersonData;

  const administrationMock = {
    username: 'some@email.com',
    password: '',
    type: '',
    person: personMock,
  }as UserData;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdministrationCardComponent);
    component = fixture.componentInstance;
    component.administration = administrationMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the username of administration', ()=>{
    expect(fixture.nativeElement.textContent).toContain(administrationMock.username);
  });

  it('should show the dni of administration', ()=>{
    expect(fixture.nativeElement.textContent).toContain(administrationMock.person.dni);
  });

  it('should show the fullname of administration', ()=>{
    expect(fixture.nativeElement.textContent).toContain(component.fullname());
  });
});
