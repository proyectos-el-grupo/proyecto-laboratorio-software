import { Placeholder } from '@angular/compiler/src/i18n/i18n_ast';
import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { Place } from '@core/models/place';
import { Genre } from '@core/modules/books/models/genre';

export interface PersonData extends BackEndDataModel {
    name: string;
    lastname: string;
    dni: string;
    birthdate: Date;
    birthplace: Place;
    address: string;
    city: Place;
    gender: string;
    ltPreferences: Genre[];
}
