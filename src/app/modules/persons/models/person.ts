import { FrontEndModel } from '@core/models/front-end-model';
import { Place } from '@core/models/place';
import { Genre } from '@core/modules/books/models/genre';
import { PersonData } from '../interfaces/person-data';

export class Person extends FrontEndModel implements PersonData {
    public name: string;
    public lastname: string;
    public dni: string;
    public birthdate: Date;
    public birthplace: Place;
    public address: string;
    public city: Place;
    public gender: string;
    public ltPreferences: Genre[];

    public get fullname() {
        return this.name + ' ' + this.lastname;
    }

    public constructor(entity: PersonData){
        super(entity);
        this.dni = entity.dni;
        this.name = entity.name;
        this.lastname = entity.lastname;
        this.birthdate = entity.birthdate;
        this.birthplace = entity.birthplace;
        this.address = entity.address;
        this.city = entity.city;
        this.gender = entity.gender;
        this.ltPreferences = entity.ltPreferences;
    }
}