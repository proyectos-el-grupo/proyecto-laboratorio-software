import { TestBed } from '@angular/core/testing';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { personMock } from 'tests/mocks/modules/persons';
import { PersonData } from '../interfaces/person-data';
import { Person } from '../models/person';

import { PersonsService } from './persons.service';

describe('PersonsService', () => {
  let httpClientSpy: { post: jasmine.Spy, put: jasmine.Spy, get: jasmine.Spy };
  let service: PersonsService;

  const className = 'Person';
  const APIEndpoint = API_BASE + '/classes/' + className;
  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'post']);
    service = new PersonsService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a single object with the id', async ()=>{
    const objectId = '1212121212121212121212';
    const endpoint = APIEndpoint + '/' + objectId;
    const mock = Object.assign({objectId}, personMock) as PersonData;

    let returnedObject;

    httpClientSpy.get.and.returnValue(asyncData(mock));

    returnedObject = await service.getOne(objectId);

    expect(returnedObject).toBeInstanceOf(Person);
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('should create a Person with pointers to Place and Genre', async () => {
    const person = new Person(personMock);
    const backendPerson = {
      name: person.name,
      lastname: person.lastname,
      dni: person.dni,
      birthdate: person.birthdate,
      birthplace: {
        __type: 'Pointer',
        className: 'Place',
        objectId: person.birthplace.id,
      },
      address: person.address,
      city: {
        __type: 'Pointer',
        className: 'Place',
        objectId: person.city.id,
      },
      gender: person.gender,
      ltPreferences: [
        {
          __type:'Pointer',
          className: 'Genre',
          objectId: person.ltPreferences[0].id,
        },
        {
          __type:'Pointer',
          className: 'Genre',
          objectId: person.ltPreferences[1].id,
        },
        {
          __type:'Pointer',
          className: 'Genre',
          objectId: person.ltPreferences[2].id,
        },
      ],
    };
    const endpoint = API_BASE + '/classes/Person';

    httpClientSpy.post.and.returnValue(asyncData({objectId: '1a1a1a1a1a'}));

    const returnedObject = await service.createPerson(person);

    expect(returnedObject).toEqual({objectId: '1a1a1a1a1a'});
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.post.calls.first().args[1]).toEqual(backendPerson);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
  });

  it('should Create a Person only with dni, name and lastname', async ()=>{
    const personAdminMock = {
      name: 'someName',
      lastname: 'somelastname',
      dni: '0123654789'
    }as any as PersonData;

    const person = new Person(personAdminMock);
    
    const backendPerson = {
      name: person.name,
      lastname: person.lastname,
      dni: person.dni,
      birthdate: null,
      birthplace: null,
      address: null,
      gender: null,
      city: null,
      ltPreferences: null
    };
    const endpoint = API_BASE + '/classes/' + className;

    httpClientSpy.post.and.returnValue(asyncData({objectId: '1a1a1a1a1a'}));

    const returnedObject = await service.createAdmin(person);

    expect(returnedObject).toEqual({objectId: '1a1a1a1a1a'});
    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.post.calls.first().args[1]).toEqual(backendPerson);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
  });

  it('should edit person of a client user', async ()=>{
    const objectId = '1212121212';
    const person = Object.assign({id: objectId}, personMock) as Person;
    const backendPerson = {
      name: person.name,
      lastname: person.lastname,
      dni: person.dni,
      birthdate: person.birthdate,
      birthplace: {
        __type: 'Pointer',
        className: 'Place',
        objectId: person.birthplace.id,
      },
      address: person.address,
      city: {
        __type: 'Pointer',
        className: 'Place',
        objectId: person.city.id,
      },
    };

    const endpoint = API_BASE + '/classes/' + className + '/' + objectId ;

    httpClientSpy.put.and.returnValue(asyncData({updateAt: new Date(1999,1,1)}));

    const returnedObject = await service.edit(person);

    expect(returnedObject).toEqual({updateAt: new Date(1999,1,1)});
    expect(httpClientSpy.put).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.put.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.put.calls.first().args[1]).toEqual(backendPerson);
    expect(httpClientSpy.put.calls.first().args[2]).toEqual({headers: APIHeaders});
  });
});
