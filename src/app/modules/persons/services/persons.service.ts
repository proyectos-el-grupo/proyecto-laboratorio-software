import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DataAPIService } from '@core/services/data-api.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { PersonData } from '../interfaces/person-data';
import { Person } from '../models/person';

@Injectable({
  providedIn: 'root'
})
export class PersonsService extends DataAPIService<Person> {
  public get className() {
    return 'Person';
  }

  public modelConstructor<PersonData>(data){
    return new Person(data);
  }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data);
  }

  /**
  * Create a Person with relations
  */
  public createPerson(person: PersonData){
    const APIEndPoint = API_BASE + '/classes/Person';
    const APIHeader = {
      headers: API_HEADERS,
    };
    const ltPreferences = [];

    person.ltPreferences.forEach((element, i) => {
      let preference = {
        __type:'Pointer',
        className: 'Genre',
        objectId: person.ltPreferences[i].id,
      };
      ltPreferences.push(preference);
    });

    const backendPerson = {
      name: person.name,
      lastname: person.lastname,
      dni: person.dni,
      birthdate: person.birthdate,
      birthplace: {
        __type: 'Pointer',
        className: 'Place',
        objectId: person.birthplace.id,
      },
      address: person.address,
      city: {
        __type: 'Pointer',
        className: 'Place',
        objectId: person.city.id,
      },
      gender: person.gender,
      ltPreferences: ltPreferences,
    };
  
    return this.httpClient.post<{objectId}>(APIEndPoint, backendPerson, APIHeader)
      .toPromise();
    }

    public createAdmin(person: PersonData){
      const APIEndPoint = API_BASE + '/classes/' + this.className;
      const APIHeader = {
        headers: API_HEADERS
      };

      const backendPerson = {
        name: person.name,
        lastname: person.lastname,
        dni: person.dni,
        birthdate: null,
        birthplace: null,
        address: null,
        gender: null,
        city: null,
        ltPreferences: null
      };

      return this.httpClient.post<{objectId}>(APIEndPoint, backendPerson, APIHeader)
      .toPromise();
    }

    public edit(person: Person){
      const APIEndpoint = API_BASE + '/classes/'+ this.className + '/' + person.id;
      const APIHeader = {
        headers: API_HEADERS,
      };
      let backendPerson;

      backendPerson = {
        name: person.name,
        lastname: person.lastname,
        dni: person.dni,
        birthdate: person.birthdate,
        birthplace: {
          __type: 'Pointer',
          className: 'Place',
          objectId: person.birthplace.id,
        },
        address: person.address,
        city: {
          __type: 'Pointer',
          className: 'Place',
          objectId: person.city.id,
        },
      };
      

      return this.httpClient.put<{updateAt}>(APIEndpoint, backendPerson, APIHeader)
      .toPromise();
    }

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }
}
