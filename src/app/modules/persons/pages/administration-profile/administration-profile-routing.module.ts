import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdministrationProfilePage } from './administration-profile.page';

const routes: Routes = [
  {
    path: '',
    component: AdministrationProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationProfilePageRoutingModule {}
