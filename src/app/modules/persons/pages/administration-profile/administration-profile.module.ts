import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdministrationProfilePageRoutingModule } from './administration-profile-routing.module';

import { AdministrationProfilePage } from './administration-profile.page';
import { PersonalDataFormPage } from '@core/modules/administration/pages/personal-data-form/personal-data-form.page';
import { PersonalDataFormComponent } from '@core/modules/administration/components/personal-data-form/personal-data-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdministrationProfilePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    AdministrationProfilePage,
  ]
})
export class AdministrationProfilePageModule {}
