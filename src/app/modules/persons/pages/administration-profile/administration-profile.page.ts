import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonalDataFormPage } from '@core/modules/administration/pages/personal-data-form/personal-data-form.page';
import { AdministrationService } from '@core/modules/administration/services/administration.service';
import { User } from '@core/modules/authentication/models/user';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { PlacesService } from '@core/services/places.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-administration-profile',
  templateUrl: './administration-profile.page.html',
  styleUrls: ['./administration-profile.page.scss'],
})
export class AdministrationProfilePage implements OnInit, OnDestroy {
  public user: User;
  public userData: User;

  constructor(
    public modalController: ModalController,
    private authenticationService: AuthenticationService,
    private administrationService: AdministrationService,
    private placesService: PlacesService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.verifySession();
    this.loadUser();
  }

  @HostListener('unloaded')
  ngOnDestroy(){
  }

  public async loadUser(){
    this.user = await this.administrationService.getOneUser(this.authenticationService.userId);
    this.userData = this.user;
    this.user.person.birthplace = await this.placesService.getOne(this.userData.person.birthplace.objectId);
    this.user.person.city = await this.placesService.getOne(this.userData.person.city.objectId);
  }

  public verifySession(){
    if(this.authenticationService.isAuthenticated && 
       this.authenticationService.userType === 'root'){
      this.router.navigateByUrl('/search-book', {skipLocationChange: false});

    }else if(!this.authenticationService.isAuthenticated){
      this.router.navigateByUrl('/login', {skipLocationChange: false});
    }
  }

  public async goToEditProfile(){
    const modaEdit = await this.modalController.create({
      component: PersonalDataFormPage,
      componentProps: {
        user: this.userData, 
      }
    });
    await modaEdit.present();
  }
  
  public goToChangePassword(){
    return;
  }
}
