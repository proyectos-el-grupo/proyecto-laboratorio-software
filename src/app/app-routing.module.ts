import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'search-book',
    pathMatch: 'full'
  },
  {
    path: 'signup',
    loadChildren: () => import('./modules/authentication/pages/signup/signup.module')
                              .then( m => m.SignupPageModule)
  },
  {
    path: 'administration',
    loadChildren: () => import('./modules/administration/pages/administration-list/administration-list.module')
                              .then( m => m.AdministrationListPageModule)
  },
  {
    path: 'administration-form',
    loadChildren: () => import('./modules/administration/pages/administration-form/administration-form.module').then( m => m.AdministrationFormPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./modules/authentication/pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'admin-books',
    loadChildren: () => import('./modules/books/pages/admin-books/admin-books.module').then( m => m.AdminBooksPageModule)
  },
  {
    path: 'search-book',
    loadChildren: () => import('./modules/books/pages/search-book/search-book.module').then( m => m.SearchBookPageModule)
  },
  {
    path: 'book-view',
    loadChildren: () => import('./modules/books/pages/book-view/book-view.module').then( m => m.BookViewPageModule)
  },
  {
    path: 'personal-data',
    loadChildren: () => import('./modules/administration/pages/personal-data-form/personal-data-form.module').then( m => m.PersonalDataFormPageModule)
  },
  {
    path: 'search-book-view',
    loadChildren: () => import('./modules/books/pages/search-book-view/search-book-view.module').then( m => m.SearchBookViewPageModule)
  },
  {
    path: 'administration-profile',
    loadChildren: () => import('./modules/persons/pages/administration-profile/administration-profile.module').then( m => m.AdministrationProfilePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
