import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Place } from '@core/models/place';
import { DataAPIService } from './data-api.service';

@Injectable({
  providedIn: 'root'
})
export class PlacesService extends DataAPIService<Place> {
  public get className() {
    return 'Place';
  }

  public modelConstructor<PlaceData>(data) {
    return new Place(data);
  }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data)
  }

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
   }
}
