import { Place } from '@core/models/place';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';

import { PlacesService } from './places.service';

describe('PlacesService', () => {
  let httpClientSpy: { post: jasmine.Spy, patch: jasmine.Spy, get: jasmine.Spy };
  let service: PlacesService;

  const className = 'Place';
  const APIEndpoint = API_BASE + '/classes/' + className;
  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
    service = new PlacesService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected objects from server', async ()=>{
    const placesMock = [
      {
        city: 'Dosquebradas',
        state: 'Risaralda'
      },
      {
        city: 'Pereira',
        state: 'Risaralda'
      }
    ];

    const placesResults = {
      results: placesMock,
    };

    let returnedObjects;

    httpClientSpy.get.and.returnValue(asyncData(placesResults));

    returnedObjects = await service.getAll();

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(Place);
      expect(element).toEqual(jasmine.objectContaining(placesMock[i]));
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(APIEndpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
  });
});
