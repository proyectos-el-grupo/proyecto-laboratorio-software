import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FileData } from '@core/interfaces/file-data';

import { API_BASE, API_HEADERS_JPG } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  public get className() {
    return 'files';
  }

  /**
  * The API endpoint URL.
  */
  private get APIEndpoint(): string {
    return API_BASE + '/' + this.className;
  }

  /**
   * The HTTPClient request options
  */
  private get requestOptions() {
    return {
      headers: API_HEADERS_JPG
    };
  }

  constructor(public httpClient: HttpClient) { }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public async savePicture(data){
    return this.httpClient['post']<FileData>(this.APIEndpoint, data, this.requestOptions)
        .toPromise()
        .catch(this.handleRequestError);
}
}
