import { API_BASE, API_HEADERS, API_HEADERS_JPG } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { fileMock } from 'tests/mocks/modules/files';
import { FilesService } from './files.service';

describe('FilesService', () => {
  let httpClientSpy: {
    get: jasmine.Spy,
    put: jasmine.Spy,
    post: jasmine.Spy,
    delete: jasmine.Spy
  };
  let service: FilesService;

  const className = 'files';
  const APIEndPoint = API_BASE + '/' + className;
  const APIHeaders = API_HEADERS_JPG;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'post', 'delete']);
    service = new FilesService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save a picture in database', async () => {
    let returnedObject;

    let mockImage = {};

    httpClientSpy.post.and.returnValue(asyncData(fileMock));

    returnedObject = await service.savePicture(mockImage);

    expect(returnedObject).toEqual(fileMock);
    expect(httpClientSpy.put).toHaveBeenCalledTimes(0);
    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(APIEndPoint);
    expect(httpClientSpy.post.calls.first().args[1]).toEqual(mockImage);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
  });
});
