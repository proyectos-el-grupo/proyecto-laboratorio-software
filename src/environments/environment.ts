// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

/* Backend local para ser usado en con docker Compose
export const API_BASE = 'http://localhost:1337/parse';
export const API_HEADERS = {
    'X-Parse-Application-Id': 'my.app.id',
    'Content-Type': 'application/json',
};
*/

export const API_BASE = 'https://parseapi.back4app.com';
export const API_HEADERS = {
    'X-Parse-Application-Id': 'U4noYv417O0e0O0rei1yJagQrLjQIsOVqkY6cgqX',
    'X-Parse-REST-API-Key': 'gWgVSFRNWaABLGbKXXBWAhChLIkfj59TMxbNSD3l',
    'Content-Type': 'application/json',
};
export const API_HEADERS_JPG = {
  'X-Parse-Application-Id': 'U4noYv417O0e0O0rei1yJagQrLjQIsOVqkY6cgqX',
  'X-Parse-REST-API-Key': 'gWgVSFRNWaABLGbKXXBWAhChLIkfj59TMxbNSD3l',
  'Content-Type': 'image/jpeg',
};
