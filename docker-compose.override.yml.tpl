version: '3'
services:

  dashboard:
    image: parseplatform/parse-dashboard
    ports:
      - 4040:4040
    environment:
      - PARSE_DASHBOARD_SERVER_URL
      - PARSE_DASHBOARD_APP_ID
      - PARSE_DASHBOARD_MASTER_KEY
      - PARSE_DASHBOARD_APP_NAME
      - PARSE_DASHBOARD_ALLOW_INSECURE_HTTP
      - PARSE_DASHBOARD_USER_ID
      - PARSE_DASHBOARD_USER_PASSWORD
