<!---------------------------------------------------------------------------->
<!-- Copy the base requirement here -->
Requerimiento Base: REQ-{N} {Descripción}

<!--
Definir historia de usuario:
-->

> Para poder <función> como <rol>, puedo <objetivos>.

<!---------------------------------------------------------------------------->
# Tareas
<!--
¿Cuales tareas necesitan ser hechas para cumplir este requerimiento?

Las tareas deben ser definidas así:

* [ ] #{N} {Descripción}

donde:

* {N} es el número asignado al issue.
* {Description} es la descripción o título del issue al cual se hace referencia.
-->

* [ ] ~"Tipo::Refinamiento" Tarea: #nnn
* [ ] #nnn Tarea 1
* [ ] #nnn Tarea 2
* [ ] #nnn Tarea 3

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Tipo::Requerimiento de Usuario" ~"Estado::Nuevo"

