<!---------------------------------------------------------------------------->
<!-- Describir la neva caracteristica -->
~"Tipo::Requerimiento de Usuario": #{N de ~"Tipo::Requerimiento de Usuario"}
/relate #{N of ~"Tipo::Requerimiento de Usuario"}



<!---------------------------------------------------------------------------->
# Tareas
<!--
Definir una por una las tareas para terminar este issue.
El formato debería ser:

* [ ] Nombre de la tarea `tiempo estimado expresado en m o h`

Si la tarea toma mas de un día, esta debería ser entonces dividida en diferentes "issues".
-->

* [ ] Tarea 1 `1h`
* [ ] Tarea 2 `30min`
* [ ] Tarea 3 `1.5h`

<!---------------------------------------------------------------------------->
# Definición de hecho
<!--
¿Que criterios deberia tener la implementación para considerarse hecha?

Ver: https://www.agilealliance.org/glossary/definition-of-done/
-->

* [ ] Primer criterio.
* [ ] Segundo criterio.

<!---------------------------------------------------------------------------->
# Información Adicional

<!--........................................................................-->
## Propuesta de diseño



<!--........................................................................-->
## Notas de diseño



<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Tipo::Caracteristica" ~"Estado::Nuevo"

