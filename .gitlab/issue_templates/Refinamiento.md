<!---------------------------------------------------------------------------->
<!-- Context -->
Base Issue: #{N} {Issue description}



<!---------------------------------------------------------------------------->
# Tarea

* [ ] Contextualizar el requerimiento.
* [ ] Relacionar issues existentes.
* [ ] Listar los archivos relacionados al issue.
* [ ] Revisar las entidades relacionadas en base de datos y describir como se relacionan.
* [ ] Revisar el código existente y analizar los algoritmos.
* [ ] Listar bibliotecas externas requeridas y describir como pueden ser usadas.

<!---------------------------------------------------------------------------->
# Archivos relacionados

* `app/file/1.ts`
* `app/file/2.ts`
* `app/file/3.ts`

<!---------------------------------------------------------------------------->
# Entidades en base de datos

* `NombreEntidad` Es donde algunos registros son guardados.
```ts
interface NombreEntidad {
    aString: string;
    aNumber: number;
    aBoolean: boolean;
}
```

* `EntidadCompuesta`: Algunas otras caracteristicas relacionadas guardadas.
```ts
interface ComposistedEntity {
    anEntity: NombreEntidad;
}
```

<!---------------------------------------------------------------------------->
# Analisis del algoritmo
<!--
* Lista de carcateristicas.
* Algunas notas relevantes.

```
Code example
```

Usar formato de su preferencia.
-->



<!---------------------------------------------------------------------------->
# Bibliotecas externas requeridas
<!--

Lista de todas las bibiotecas requeridas y descripción de porqué son requeridas y 
si hay otras alternativas.
-->

* Biblioteca A: Se necesita para hacer A tarea. Funciona mejor que:
  * [ALT] Biblioteca B: Esta tiene Y problema y no ha sido resuelto.
  * [ALT] Biblioteca C: Esta tiene Z problema y no ha sido resuelto.
* Biblioteca D: bla, bla, bla. **Sin alternativas**.

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Tipo::Refinamiento" ~"Estado::Nuevo"

