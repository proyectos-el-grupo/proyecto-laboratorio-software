<!---------------------------------------------------------------------------->
<!-- Describa brevemente el error/bug/problema -->



<!---------------------------------------------------------------------------->
# Modulo/Componente/Servicio
<!--
- ¿Cual elemento está siendo afectado?
-->



<!---------------------------------------------------------------------------->
# Pasos para reproducir
<!--
- Sea lo mas descriptivo posible y trate de reproducir el bug mas de una vez.
-->

1. Iniciar Sesión
2. Ir a Tal Parte

<!---------------------------------------------------------------------------->
# Resultado actual (Pantallazo, informe de error)
<!--
- Plantilla de pantallazos:
-
- | Titulo  |    Descripción    |               Imagen              |
- |--------|-------------------|-----------------------------------|
- |Mi titulo|Breve Descripción|![Image](https://image.url/file.ext)
-
-->


<!---------------------------------------------------------------------------->
# Resultado esperado
<!--
- ¿Qué debería pasar en el caso bueno?
-->



<!---------------------------------------------------------------------------->
# Información Adicional

<!--........................................................................-->
# Posible causa del error
<!--
- ¿Vez algún patrón o alguna causa del error?
-->



<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Tipo::Bug" ~"Estado::Nuevo"

