<!---------------------------------------------------------------------------->
<!-- Describe el requerimiento -->



<!---------------------------------------------------------------------------->
# Definición de hecho
<!--
- ¿Qué criterios debe cumplir para considerarse realizado?
-
- Ver: https://www.agilealliance.org/glossary/definition-of-done/
-->

* [ ] Primer Criterio.
* [ ] Segundo Criterio.

<!---------------------------------------------------------------------------->
# Información Adicional

<!--........................................................................-->
## Propuesta de diseño



<!--........................................................................-->
## Notas de diseño 



<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Tipo::Requerimiento Interno" ~"Estado::Nuevo"

